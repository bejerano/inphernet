import sys, os
import pickle
from phrank import Phrank

gene_index = 0
gene_ref = {}
disease_index = 0
disease_ref = {}
pheno_index = 0
pheno_ref = {}
pathway_index = 0
pathway_ref = {}
uberon_index = 0
uberon_ref = {}
gene_uberon = {} # key = gene, val = uberon
gene_pathway = {} #key = gene, val = pathway
pathway_gene = {} #key = pathway, val = gene
gene_disease = {} #key = gene, val = disease
gene_pheno = {} # key = gene, val = pheno
disease_pheno = {} # key = disease, val = pheno
upheno = {} #key = child, val = parent
uberon = {} #key = child, val = parent
gene_interaction = {} #key = gene, val = interacting genes

#Phrank( dag (child then parent), gene to pheno, blank) 

for i in open("output/human_gene_disease_pheno_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    gene_id = i[2].strip('"').split('/')[-1].replace(':', '_')
    #gene_name = i[3].strip('"')
    gene_names = i[3].strip('[').strip(']').replace('"','').split(', ')
    disease = i[8].strip('"').split('/')[-1].replace(':', '_')
    disease_name = i[9].strip('"')
    pheno = i[11].strip('"').split('/')[-1].replace(':', '_')
    pheno_name = i[12].strip('"')
    org = "NCBITaxon_9606"
    
    for j in gene_names: 
        gene = "%s*%s" %(j, org)
        if gene not in gene_ref:
            gene_ref[gene] = [str(gene_index), gene_id]
            gene_index += 1

    if pheno not in pheno_ref:
        pheno_ref[pheno] = str(pheno_index)
        pheno_index += 1
    if disease not in disease_ref:
        disease_ref[disease] = str(disease_index)
        disease_index += 1

    gene_tags = [gene_ref["%s*%s" %(x, org)][0] for x in gene_names]
    pheno_tag = pheno_ref[pheno]
    disease_tag = disease_ref[disease]
    for gene_tag in gene_tags:
        if gene_tag not in gene_disease:
            gene_disease[gene_tag] = set()
        gene_disease[gene_tag].add(disease_tag)
        
        if gene_tag not in gene_pheno:
            gene_pheno[gene_tag] = set()
        gene_pheno[gene_tag].add(pheno_tag)
    if disease_tag not in disease_pheno:
        disease_pheno[disease_tag] = set()
    disease_pheno[disease_tag] = pheno_tag

for i in open("output/human_gene_to_gene_interaction_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    gene1_id = i[2].strip('"').split('/')[-1].replace(':', '_')
    gene1_names = i[3].strip('[').strip(']').replace('"','').split(', ')
    gene2_id = i[4].strip('"').split('/')[-1].replace(':', '_')
    gene2_names = i[5].strip('[').strip(']').replace('"','').split(', ')
    org = "NCBITaxon_9606"

    g1_tags = []
    g2_tags = []
    for j in gene1_names:
        gene = "%s*%s" %(j, org)
        if gene not in gene_ref:
            gene_ref[gene] = [str(gene_index), gene1_id]
            gene_index += 1
        g1_tags.append(gene_ref[gene][0])
    for j in gene2_names:
        gene = "%s*%s" %(j, org)
        if gene not in gene_ref:
            gene_ref[gene] = [str(gene_index), gene2_id]
            gene_index += 1
        g2_tags.append(gene_ref[gene][0])
    for j in g1_tags:
        for k in g2_tags:
            if j not in gene_interaction:
                gene_interaction[j] = set()
            if k not in gene_interaction:
                gene_interaction[k] = set()
            gene_interaction[j].add(k)
            gene_interaction[k].add(j)

for i in open("output/human_gene_uberon_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    gene_id = i[0].strip('"').split('/')[-1].replace(':', '_')
    gene_names = i[1].strip('[').strip(']').replace('"','').split(', ')
    uberon_id = i[2].strip('"').split('/')[-1].replace(':', '_')
    uberon_names = i[3].strip('"')
    org = "NCBITaxon_9606"

    if uberon_id not in uberon_ref:
        uberon_ref[uberon_id] = str(uberon_index)
        uberon_index += 1

    for j in gene_names:
        gene = "%s*%s" %(j, org)
        if gene not in gene_ref:
            gene_ref[gene] = [str(gene_index), gene_id]
            gene_index += 1
        if gene_ref[gene][0] not in gene_uberon:
            gene_uberon[gene_ref[gene][0]] = set()
        gene_uberon[gene_ref[gene][0]].add(uberon_ref[uberon_id])

for i in open("output/mouse_gene_disease_pheno_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    org = i[0].strip('"').split('/')[-1]
    gene_id = i[2].strip('"').split('/')[-1].replace(':','_')
    gene_name = i[3].strip('"')
    pheno = i[4].strip('"').split('/')[-1].replace(':','_')
    pheno_name = i[5].strip('"')
    
    gene = "%s*%s" %(gene_name, org)
    if gene not in gene_ref:
        gene_ref[gene] = [str(gene_index), gene_id]
        gene_index += 1

    if pheno not in pheno_ref:
        pheno_ref[pheno] = str(pheno_index)
        pheno_index += 1

    gene_tag = str(gene_ref[gene][0])
    if gene_tag not in gene_pheno:
        gene_pheno[gene_tag] = set()
    gene_pheno[gene_tag].add(pheno_ref[pheno])


for i in open("output/zfish_gene_disease_pheno_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    org = i[0].strip('"').split('/')[-1]
    gene_id = i[2].strip('"').split('/')[-1].replace(':','_')
    gene_name = i[3].strip('"')
    pheno = i[4].strip('"').split('/')[-1].replace(':','_')
    pheno_name = i[5].strip('"')
    
    gene = "%s*%s" %(gene_name, org)
    if gene not in gene_ref:
        gene_ref[gene] = [str(gene_index), gene_id]
        gene_index += 1

    if pheno not in pheno_ref:
        pheno_ref[pheno] = str(pheno_index)
        pheno_index += 1

    gene_tag = gene_ref[gene][0]
    if gene_tag not in gene_pheno:
        gene_pheno[gene_tag] = set()
    gene_pheno[gene_tag].add(pheno_ref[pheno])

for i in open("output/gene_pathway_human_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    org = i[0].strip('"').split('/')[-1]
    gene_id = i[1].strip('"').split('/')[-1].replace(':','_')
    gene_names = i[2].strip('[').strip(']').replace('"','').split(', ')
    pathway_id = i[3].strip('"').split('/')[-1].split('-')[-1]
    pathway_name = i[4].strip('[').strip(']').replace('"','').split(', ')
    
    if pathway_id not in pathway_ref:
        pathway_ref[pathway_id] = str(pathway_index)
        pathway_index += 1

    for gene_name in gene_names:
        gene = "%s*%s" %(gene_name, org)
        if gene not in gene_ref:
            gene_ref[gene] = [str(gene_index), gene_id]
            gene_index += 1
        if gene_ref[gene][0] not in gene_pathway:
            gene_pathway[gene_ref[gene][0]] = set()
        gene_pathway[gene_ref[gene][0]].add(pathway_ref[pathway_id])

for i in open("output/gene_pathway_mouse_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    org = i[0].strip('"').split('/')[-1]
    gene_id = i[1].strip('"').split('/')[-1].replace(':','_')
    gene_names = i[2].strip('[').strip(']').replace('"','').split(', ')
    pathway_id = i[3].strip('"').split('/')[-1].split('-')[-1]
    pathway_name = i[4].strip('[').strip(']').replace('"','').split(', ')
    
    if pathway_id not in pathway_ref:
        pathway_ref[pathway_id] = str(pathway_index)
        pathway_index += 1

    for gene_name in gene_names:
        gene = "%s*%s" %(gene_name, org)
        if gene not in gene_ref:
            gene_ref[gene] = [str(gene_index), gene_id]
            gene_index += 1
        if gene_ref[gene][0] not in gene_pathway:
            gene_pathway[gene_ref[gene][0]] = set()
        gene_pathway[gene_ref[gene][0]].add(pathway_ref[pathway_id])


for i in open("output/gene_pathway_zfish_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    org = i[0].strip('"').split('/')[-1]
    gene_id = i[1].strip('"').split('/')[-1].replace(':','_')
    gene_names = i[2].strip('[').strip(']').replace('"','').split(', ')
    pathway_id = i[3].strip('"').split('/')[-1].split('-')[-1]
    pathway_name = i[4].strip('[').strip(']').replace('"','').split(', ')
    
    if pathway_id not in pathway_ref:
        pathway_ref[pathway_id] = str(pathway_index)
        pathway_index += 1

    for gene_name in gene_names:
        gene = "%s*%s" %(gene_name, org)
        if gene not in gene_ref:
            gene_ref[gene] = [str(gene_index), gene_id]
            gene_index += 1
        if gene_ref[gene][0] not in gene_pathway:
            gene_pathway[gene_ref[gene][0]] = set()
        gene_pathway[gene_ref[gene][0]].add(pathway_ref[pathway_id])


for i in open("output/uberon_is_a_uberon_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    uberon1 = i[0].strip('"').split('/')[-1] #child
    name1 = i[1]
    uberon2 = i[2].strip('"').split('/')[-1] #parent
    name2 = i[3]
    if uberon1 not in uberon_ref:
        uberon_ref[uberon1] = str(uberon_index)
        uberon_index += 1
    if uberon2 not in uberon_ref:
        uberon_ref[uberon2] = str(uberon_index)
        uberon_index += 1
    if uberon_ref[uberon1] not in uberon:
        uberon[uberon_ref[uberon1]] = set()
    uberon[uberon_ref[uberon1]].add(uberon_ref[uberon2])

for i in open("output/pheno_is_a_pheno_out"):
    i = i.rstrip().split('\t')
    if len(i) < 2:
        continue
    pheno1 = i[0].strip('"').split('/')[-1] #child
    name1 = i[1].strip('"')
    pheno2 = i[2].strip('"').split('/')[-1] #parent
    name2 = i[3].strip('"')
    if pheno1 not in pheno_ref:
        pheno_ref[pheno1] = str(pheno_index)
        pheno_index += 1
    if pheno2 not in pheno_ref:
        pheno_ref[pheno2] = str(pheno_index)
        pheno_index += 1
    if pheno_ref[pheno1] not in upheno:
        upheno[pheno_ref[pheno1]] = set()
    upheno[pheno_ref[pheno1]].add(pheno_ref[pheno2])




species_of_interest = ["NCBITaxon_9606", "NCBITaxon_10090", "NCBITaxon_7955"] #human, mouse, zebrafish 

by_gene_id = {}
for x in gene_ref:
    if len(gene_ref[x]) > 1:
        ref_id, onto_id = gene_ref[x]
        by_gene_id[onto_id] = ref_id

# ENSEMBL
human_xref = {}
for i in open("../ensembl/human_xref"):
    i = i.rstrip().split('\t')
    human_xref[i[1]] = [i[0].replace(':', '_'), i[2]]

mouse_xref = {}
for i in open("../ensembl/mouse_xref"):
    i = i.rstrip().split('\t')
    mouse_xref[i[1]] = [i[0].replace(':', '_'), i[2]]

zfish_xref = {}
for i in open("../ensembl/zebrafish_xref"):
    i = i.rstrip().split('\t')
    zfish_xref[i[1]] = [i[0], i[2]]

                       
bet_paralog = {}
bet_paralog_zfish = {}
paralog = {}
ortholog = {}
ortholog_zfish = {}
paralog_mouse = {}
paralog_zfish = {}
for i in open("../ensembl/ensembl_homolog"):
    g1, t1, g2, t2, hom = i.rstrip().split('\t')
    if "ortholog" in hom:
        if t1 == t2:
            continue
        g = g1 if t1 == "9606" else g2
        o = g2 if t1 == "9606" else g1
        t = t2 if t1 == "9606" else t1
        if g in human_xref:
            g, n = human_xref[g]
            if g in by_gene_id:
                g = by_gene_id[g]
            else:
                name = "%s*NCBITaxon_9606" %(n)
                gene_ref[name] = [str(gene_index), g]
                g = str(gene_index)
                gene_index += 1
        else:
            continue
        if t == "10090": #mouse
            if o in mouse_xref:
                o,n = mouse_xref[o]
                if o in by_gene_id:
                    o = by_gene_id[o]
                else:
                    name = "%s*NCBITaxon_10090" %(n)
                    gene_ref[name] = [str(gene_index), o]
                    o = str(gene_index)
                    gene_index += 1
                if g not in ortholog:
                    ortholog[g] = set()
                ortholog[g].add(o)
        elif t == "7955": #zebrafish
            if o in zfish_xref:
                o,n = zfish_xref[o]
                if o in by_gene_id:
                    o = by_gene_id[o]
                else:
                    name = "%s*NCBITaxon_7955" %(n)
                    gene_ref[name] = [str(gene_index), o]
                    o = str(gene_index)
                    gene_index += 1
                if g not in ortholog_zfish:
                    ortholog_zfish[g] = set()
                ortholog_zfish[g].add(o)
    elif hom == "within_species_paralog":
        if g1 in human_xref and g2 in human_xref:
            g1, n1 = human_xref[g1]
            g2, n2 = human_xref[g2]
            if g1 not in by_gene_id:
                name = "%s*NCBITaxon_9606" %(n1)
                gene_ref[name] = [str(gene_index), g1]
                g1 = str(gene_index)
                gene_index += 1
            else:
                g1 = by_gene_id[g1]

            if g2 not in by_gene_id:
                name = "%s*NCBITaxon_9606" %(n2)
                gene_ref[name] = [str(gene_index), g2]
                g2 = str(gene_index)
                gene_index += 1
            else:
                g2 = by_gene_id[g2]  

            if g1 not in paralog:
                paralog[g1] = set()
            if g2 not in paralog:
                paralog[g2] = set()
            paralog[g1].add(g2)
            paralog[g2].add(g1)
        elif g1 in mouse_xref and g2 in mouse_xref:
            g1, n1 = mouse_xref[g1]
            g2, n2 = mouse_xref[g2]
            if g1 not in by_gene_id:
                name = "%s*NCBITaxon_10090" %(n1)
                gene_ref[name] = [str(gene_index), g1]
                g1 = str(gene_index)
                gene_index += 1
            else:
                g1 = by_gene_id[g1]

            if g2 not in by_gene_id:
                name = "%s*NCBITaxon_10090" %(n2)
                gene_ref[name] = [str(gene_index), g2]
                g2 = str(gene_index)
                gene_index += 1
            else:
                g2 = by_gene_id[g2]  

            if g1 not in paralog_mouse:
                paralog_mouse[g1] = set()
            if g2 not in paralog_mouse:
                paralog_mouse[g2] = set()
            paralog_mouse[g1].add(g2)
            paralog_mouse[g2].add(g1)
        elif g1 in zfish_xref and g2 in zfish_xref:
            g1, n1 = zfish_xref[g1]
            g2, n2 = zfish_xref[g2]
            if g1 not in by_gene_id:
                name = "%s*NCBITaxon_7955" %(n1)
                gene_ref[name] = [str(gene_index), g1]
                g1 = str(gene_index)
                gene_index += 1
            else:
                g1 = by_gene_id[g1]

            if g2 not in by_gene_id:
                name = "%s*NCBITaxon_7955" %(n2)
                gene_ref[name] = [str(gene_index), g2]
                g2 = str(gene_index)
                gene_index += 1
            else:
                g2 = by_gene_id[g2]  

            if g1 not in paralog_zfish:
                paralog_zfish[g1] = set()
            if g2 not in paralog_zfish:
                paralog_zfish[g2] = set()
            paralog_zfish[g1].add(g2)
            paralog_zfish[g2].add(g1)
for x in ortholog:
    betw = set(xxx for xx in ortholog[x] if xx in paralog_mouse for xxx in paralog_mouse[xx]).difference(ortholog[x])
    if x not in bet_paralog:
        bet_paralog[x] = set()
    bet_paralog[x].update(betw)
    
for x in ortholog_zfish:
    betw = set(xxx for xx in ortholog_zfish[x] if xx in paralog_zfish for xxx in paralog_zfish[xx]).difference(ortholog_zfish[x])
    if x not in bet_paralog_zfish:
        bet_paralog_zfish[x] = set()
    bet_paralog_zfish[x].update(betw)


for i in gene_pathway:
    for j in gene_pathway[i]:
        if j not in pathway_gene:
            pathway_gene[j] = set()
        pathway_gene[j].add(str(i))


pickle.dump(gene_ref, open("gene_ref.pkl", 'wb'))
pickle.dump(disease_ref, open("disease_ref.pkl", 'wb'))
pickle.dump(pheno_ref, open("pheno_ref.pkl", 'wb'))
pickle.dump(pathway_ref, open("pathway_ref.pkl", 'wb'))
pickle.dump(uberon_ref, open("uberon_ref.pkl", 'wb'))
pickle.dump(gene_uberon, open("gene_uberon.pkl", 'wb'))
pickle.dump(gene_pathway, open("gene_pathway.pkl", 'wb'))
pickle.dump(pathway_gene, open("pathway_gene.pkl", 'wb'))
pickle.dump(gene_disease, open("gene_disease.pkl", 'wb'))
pickle.dump(gene_pheno, open("gene_pheno.pkl", 'wb'))
pickle.dump(disease_pheno, open("disease_pheno.pkl", 'wb'))
pickle.dump(uberon, open("uberon.pkl", 'wb'))
pickle.dump(upheno, open("upheno.pkl", 'wb'))
pickle.dump(gene_interaction, open("gene_interaction.pkl", 'wb'))
pickle.dump(bet_paralog, open("out_paralog_mouse.pkl", 'wb'))
pickle.dump(bet_paralog_zfish, open("out_paralog_zfish.pkl", 'wb'))
pickle.dump(paralog, open("in_paralog.pkl", 'wb'))
pickle.dump(ortholog, open("ortholog.pkl", 'wb'))
pickle.dump(ortholog_zfish, open("ortholog_zfish.pkl", 'wb'))

out = open("temp_dag", 'w')
for i in upheno:
    for j in upheno[i]:
        out.write("%s\t%s\n" %(i, j))
out.close()
out = open("temp_gene_pheno", 'w')
for i in gene_pheno:
    for j in gene_pheno[i]:
        out.write("%s\t%s\n" %(i,j))
out.close()
 
upheno_phrank = Phrank( "temp_dag", "temp_gene_pheno", "temp_add")

pickle.dump(upheno_phrank, open("upheno_phrank.pkl", 'wb'))


out = open("temp_dag", 'w')
for i in uberon:
    for j in uberon[i]:
        out.write("%s\t%s\n" %(i, j))
out.close()
out = open("temp_gene_pheno", 'w')
for i in gene_uberon:
    for j in gene_uberon[i]:
        out.write("%s\t%s\n" %(i,j))
out.close()
 
uberon_phrank = Phrank( "temp_dag", "temp_gene_pheno", "temp_add")

pickle.dump(uberon_phrank, open("uberon_phrank.pkl", 'wb'))


pheno_ref_by_id = {}
for i in pheno_ref:
    pheno_ref_by_id[pheno_ref[i]] = i
pickle.dump(pheno_ref_by_id, open("pheno_ref_by_id.pkl", 'wb'))   


temp = pickle.load(open("gene_ref.pkl", 'rb'))
#temp2 = pickle.load(open("gene_interaction_new.pkl", 'rb'))

human = []
for i in temp:
    if "NCBITaxon_9606" in i:
        human.append(temp[i][0])
pickle.dump(human, open("human_genes.pkl", 'wb'))


temp = pickle.load(open("gene_disease.pkl", 'rb'))
disease = []
for i in temp:
    if i in human:
        disease.append(i)
pickle.dump(disease, open("human_disease_genes.pkl", 'wb'))





 
