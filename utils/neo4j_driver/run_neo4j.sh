#!/bin/sh
#usage: sh run_neo4j.sh query output
#warning: make sure neo4j start has already happend

if [ ! -d Monarch ]
then
    echo "Compiled API not found. Compiling..."
    javac -Xlint -cp "./neo4j-java-driver/1.1-SNAPSHOT/*" -d . monarch.java
fi

java -cp ".:neo4j-java-driver/1.1-SNAPSHOT/*" Monarch.monarch $2 $1
#java -cp ".:neo4j-java-driver/1.1-SNAPSHOT/*" Monarch.monarch temp $1
#awk -F '\t' '{print $4 "\t" $12}' temp | sort -u > temp2
#mv temp2 temp

 
