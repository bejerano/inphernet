import sys
import pickle
import os
import __settings__
from phrank import Phrank
import gc
import copy

class Net_info_extractor: 
    def __init__( self ):
        DATA_DIR = __settings__.DATA_DIR
        self.gene_ref = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "gene_ref.pkl"), "rb"))
        self.gene_pathway = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "gene_pathway.pkl"), "rb"))
        self.pathway_gene = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "pathway_gene.pkl"), "rb"))
        self.uberon_phrank = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "uberon_phrank.pkl"), "rb"))
        self.upheno_phrank = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "upheno_phrank.pkl"), "rb"))
        self.pheno_ref = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "pheno_ref.pkl"), "rb"))
        self.disease_ref = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "disease_ref.pkl"), "rb"))
        self.gene_interactions = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "gene_interaction.pkl"), "rb"))
        self.uberon_gene_interactions = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "uberon_gene_interactions.pkl"), "rb"))
        self.disease_phenotype = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "disease_pheno.pkl"), "rb"))
        self.pheno_ref_by_id = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "pheno_ref_by_id.pkl"), 'rb'))
        self.between_paralog = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "out_paralog_mouse.pkl"), "rb"))
        self.between_paralog_zebrafish = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "out_paralog_zfish.pkl"), "rb"))
        self.paralog = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "in_paralog.pkl"), "rb"))
        self.ortholog = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "ortholog.pkl"), "rb"))
        self.ortholog_zebrafish = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "ortholog_zfish.pkl"), "rb"))
        self.gene_disease = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "gene_disease.pkl"), "rb"))
        self.human_genes = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "human_genes.pkl"), "rb"))
        self.human_disease_genes = pickle.load( open(os.path.join(DATA_DIR, "monarch_data", "human_disease_genes.pkl"), "rb"))

    def reverse( self, reverse_set ):
        new = {}
        for i in reverse_set:
            for j in reverse_set[i]:
                if j not in new:
                    new[j] = set()
                new[j].add(i)
        return new

    def disease_pheno( self, disease ):
        if disease in self.disease_phenotype:
            return self.disease_phenotype[ disease ]
        return set()

    def get_pheno( self, gene ):
        if gene in self.upheno_phrank._disease_pheno_map:
            return self.upheno_phrank._disease_pheno_map[gene]
        return []

    def get_uberon( self, gene ):
        if gene in self.uberon_phrank._disease_pheno_map:
            return self.uberon_phrank._disease_pheno_map[gene]
        return [] 

    def get_mondo( self, gene ):
        if gene in self.gene_disease:
            return self.gene_disease[gene]
        return [] 

    def get_human_pheno_only( self, set_of_pheno ):
        return_set = set()
        for i in set_of_pheno:
            if "HP" in self.pheno_ref_by_id[i]:
                return_set.add(i)
        return return_set


    def load_patient_info( self, patient_data ):
        if patient_data[0] == '': #unsolved case
            self.patient_gene = ''
            self.patient_disease = '' 
        else:
            self.patient_gene = self.convert2id( patient_data[0], self.gene_ref, "NCBITaxon_9606" )
            if patient_data[7] == "N/A":
                self.patient_disease = ''
            else:
                self.patient_disease = self.convert2id( patient_data[7], self.disease_ref )
        self.patient_candidate = self.convert2id( patient_data[8], self.gene_ref, "NCBITaxon_9606" )
        self.patient_pheno = self.convert2id( patient_data[9], self.pheno_ref )
	
    def print_patient_info( self ):
        for i in self.patient_pheno:
            print ("%s\t%s" %(i, self.convert_from_id( i, self.pheno_ref )) )


    def convert2id( self, l, ref, org = "NULL" ):
        if type(l) is list:
            return_list = []
            for i in l:
                if not org == "NULL":
                    name = "%s*%s" %(i, org)
                    if name in ref:
                        if type(ref[name]) is list:
                            return_list.append(ref[name][0])
                        else:
                            return_list.append(ref[name])
                else:
                    if i in ref:
                        if type(ref[i]) is list:
                            return_list.append(ref[i][0])
                        else:
                            return_list.append(ref[i])
            return return_list
        else:
            if not org == "NULL":
                if (l + '*' + org) in ref:
                    return ref[l + '*' + org]
                else:
                    return 0
            else:
                if type(ref[l]) is list:
                    return ref[l][0]
                else:
                    return ref[l]

    def compare_pheno_to_all_genes( self ):
        all_pheno = {}
        for i in self.human_genes:
            disease = self.get_mondo(i)
            if disease:
                max_val = -1
                val = -1
                for j in disease:
                    val = self.get_pheno_to_patient_phrank( self.disease_pheno(j) )
                    if val > max_val:
                        max_val = val
                if max_val not in all_pheno:
                    all_pheno[max_val] = set()
                all_pheno[max_val].add(i)	
        return self.sort_dict_by_key(all_pheno)

		
    def sort_dict_by_key( self, d ):
        new_dict = {}
        curr = max( d.keys() )
        rank = 1
        for i in sorted( d.keys(), reverse=True ):
            if i < curr:
                rank += 1
                curr = i
            if rank not in new_dict:
                new_dict[rank] = {}
            for j in d[i]:
                new_dict[rank][j] = i
        return new_dict
	
    def convert_from_id( self, l, ref, org = "NULL"  ):
        for i in ref:
            if type(ref[i]) is list:
                if ref[i][0] == l:
                    if not org == "NULL":
                        return i.split('*')[0]
                    else:
                        return i
            else:
                if ref[i] == l:		
                    if not org == "NULL":
                        return i.split('*')[0]
                    else:
                        return i

    def is_paralogous( self, gene1, gene2 ):	
        if gene1 in self.paralog and gene2 in self.paralog[gene1]:
            return 1
        return 0

    def get_neighbors_num_pheno( self, gene ):
        count = 0
        total_set = set()
        for i in self.uberon_gene_interactions[gene]:
            total_set.update(self.get_pheno(i))
        return len(total_set)

    def is_orthologous( self, gene1, gene2 ):
        if gene1 in self.ortholog and gene2 in self.ortholog and self.ortholog[gene2].intersection( self.ortholog[gene1] ): 
            return 1
        return 0
	
    def is_between_paralog( self, gene1, gene2 ):
        if gene1 in self.between_paralog and gene2 in self.between_paralog and (self.between_paralog[gene1].intersection(self.between_paralog[gene2]) or gene1 in self.between_paralog[gene2] or gene2 in self.between_paralog[gene1]): 
            return 1
        return 0

    def is_same_pathway( self, gene1, gene2 ):
        if gene1 in self.gene_pathway and gene2 in self.gene_pathway and self.gene_pathway[gene2].intersection(self.gene_pathway[gene1]):
            return 1
        return 0 	

    def get_max_phrank_score( self, set1, set2, phrank ): 
        return_score = 0
        for i in set1:
            for j in set2:
                if i == j:
                    continue
                if i in phrank._disease_pheno_map and j in phrank._disease_pheno_map:
                    val = phrank.compute_phenotype_match( phrank._disease_pheno_map[i], phrank._disease_pheno_map[j] )
                    if val > return_score:
                        return_score = val
        return return_score

    def get_gene_to_patient_phrank( self, gene ):
        if gene in self.upheno_phrank._disease_pheno_map:
            return self.upheno_phrank.compute_phenotype_match( self.upheno_phrank._disease_pheno_map[gene], self.patient_pheno )
        return -1

    def get_gene_to_patient_phrank_normalized( self, gene ):
        if gene in self.upheno_phrank._disease_pheno_map:
            unified = self.upheno_phrank._disease_pheno_map[gene].union(self.patient_pheno)
            return self.upheno_phrank.compute_phenotype_match( self.upheno_phrank._disease_pheno_map[gene], self.patient_pheno ) / self.upheno_phrank.compute_phenotype_match(unified, unified)
        return -1

    def get_pheno_to_patient_phrank( self, pheno ):
        if pheno:
            return self.upheno_phrank.compute_phenotype_match( pheno, self.patient_pheno )
        return -1

    def get_pheno_to_patient_phrank_normalized( self, pheno ):
        if pheno:
            unified = set(pheno).union(self.patient_pheno)
            return self.upheno_phrank.compute_phenotype_match( unified, self.patient_pheno ) / self.upheno_phrank.compute_phenotype_match(unified, unified)
        return -1



    def get_max_gene_to_patient_phrank( self, genes ):
        return_val = -1
        for i in genes:
            if i in self.upheno_phrank._disease_pheno_map:
                val =  self.upheno_phrank.compute_phenotype_match( self.upheno_phrank._disease_pheno_map[i], self.patient_pheno )
                if val > return_val:	
                    return_val = val
        return return_val
	
    def only_possible_jumps( self, threshold ): 	
        temp_ref = {}
        return_dict = {}
        for i in self.gene_interactions:
            for j in self.gene_interactions[i]:
                if i in return_dict and j in return_dict[i]:
                    continue
                if i == j:
                    continue
                index = "%s*%s" %( min(i,j), max(i,j) )
                if index not in temp_ref:
                    u = set(self.uberon_phrank._disease_pheno_map[i]).union(self.uberon_phrank._disease_pheno_map[j])
                    if not u or self.uberon_phrank.compute_phenotype_match( u, u ) == 0:
                        temp_ref[index] = 0
                    else:
                        temp_ref[index] = self.uberon_phrank.compute_phenotype_match( self.uberon_phrank._disease_pheno_map[i], self.uberon_phrank._disease_pheno_map[j]) / self.uberon_phrank.compute_phenotype_match( u, u )
                if temp_ref[index] > threshold:
                    if i not in return_dict:
                        return_dict[i] = set()
                    return_dict[i].add(j)
                    if j not in return_dict:
                        return_dict[j] = set()
                    return_dict[j].add(i)
        del temp_ref
        return return_dict

    def is_equal( self, dict1, dict2 ):
        if set(dict1.keys()).symmetric_difference(set(dict2.keys())):
            return False
        for i in dict1:
            if not dict2[i] == dict1[i]:
                return False
        return True

    def nearest_k_in( self, gene, term, k, fil, specific=True):
        return_set = set()
        if gene not in term:
            return return_set, -1
        shortest = self.shortest_possible_path( gene, fil )

        for i in shortest:
            if specific:
                if shortest[i] == k and i in term[gene]:
                    return_set.add(i)
            else:
                if shortest[i] == k:
                    return_set.add(i)
        return_set.difference_update(set(self.patient_candidate))
        return return_set, max(shortest.values())
             

    def dijsktra( self, graph, startNode, visited ):
        allNodes = set(graph.keys())
        if startNode not in graph:
            return
        currNodes = graph[startNode]
        currHops = 1
        old_visited = copy.copy(visited) 
        while set(visited.keys()).symmetric_difference(allNodes):
            nextNodes = set()
            for i in currNodes:
                if i not in visited:
                    visited[i] = currHops
                else:
                    if currHops < visited[i]:
                        visited[i] = currHops
                nextNodes.update(set(graph[i])) 
            currHops += 1
            if self.is_equal( old_visited, visited ):
                break
            del old_visited
            old_visited = copy.copy(visited)
            currNodes = nextNodes


    def shortest_possible_path( self, gene, fil ):
        path = [gene] 
        allShortestPaths = {}
        vt = { gene: 0 }
        if fil:
            self.dijsktra( self.uberon_gene_interactions, gene, vt )
        else:
            self.dijsktra( self.uberon_gene_interactions_2, gene, vt )
        return vt

    def interaction_jaccardian( self, gene1, gene2 ):
        if gene1 in self.uberon_gene_interactions and gene2 in self.uberon_gene_interactions:	
            return len(self.uberon_gene_interactions[gene1].intersection(self.uberon_gene_interactions[gene2]))/float(len(self.uberon_gene_interactions[gene1].union(self.uberon_gene_interactions[gene2])))	
        return -1 




