from collections import defaultdict
import math
from utils import load_maps, load_term_hpo, closure, load_disease_gene
import sys

class Phrank:

    @staticmethod
    def compute_information_content(annotations_map, child_to_parent_map):
        information_content, marginal_information_content = {}, {}
        annotatedGeneCt = 0
        associated_phenos = defaultdict(set)
        for gene, phenos in annotations_map.iteritems():
            annotatedGeneCt += 1
            all_ancestors = closure(phenos, child_to_parent_map)

            # for each ancestor increment the count since this pheno is now associated with the specified gene
            for pheno in all_ancestors:
                associated_phenos[pheno].add(gene)

        phenos = associated_phenos.keys()
        for pheno in phenos:
            information_content[pheno] = -math.log(1.0*len(associated_phenos[pheno])/annotatedGeneCt, 2) if len(associated_phenos[pheno]) else 0

        for pheno in phenos:
            parent_phenos = child_to_parent_map[pheno]
            parent_entropy = 0
            if len(parent_phenos) == 1:
                parent_entropy = information_content[parent_phenos[0]]
            elif len(parent_phenos) > 1:
                list_of_phenosets = [associated_phenos[parent] for parent in parent_phenos]
                parent_set = set([])
                for phenoset in list_of_phenosets:
                    parent_set = parent_set & phenoset if parent_set else phenoset
                parent_entropy = -math.log(1.0*len(parent_set)/annotatedGeneCt, 2) if len(parent_set) else 0
            marginal_information_content[pheno] = information_content[pheno] - parent_entropy
        return information_content, marginal_information_content
    
    def __init__(self, dagfile, diseaseannotationsfile, diseasegenefile):
        """Initialize Phrank object and all the variables need to run it"""
        self._child_to_parent, self._parent_to_children = load_maps(dagfile)
        self._total_pheno = set(self._child_to_parent.keys()).union(set(self._parent_to_children.keys()))
        self._disease_pheno_map = load_term_hpo(diseaseannotationsfile, self._total_pheno)
        self._disease_gene_map = load_disease_gene(diseasegenefile)
        self._IC, self._marginal_IC = Phrank.compute_information_content(self._disease_pheno_map, self._child_to_parent)
    
    def get_causal_rank(self, scores, causal_item):
        rank = 0
        former = -1
        tie = set()
        for score in scores:
            if score[0] == former:
                tie.add(score[1])
            else:
                if causal_item in tie:
                    rank += float(len(tie) + 1)/2.0
                    return score[1], score[0], rank
                rank += len(tie)
                tie = set()
                tie.add(score[1])
            former = score[0]
        return causal_item, None, len(scores) + 1

    def rank_diseases(self, patient_genes, patient_phenotypes):
        """Compute the Phrank score for each disease matching the patient phenotypes"""
        disease_scores = []
        for disease in self._disease_pheno_map:
            if self._disease_gene_map[disease] & patient_genes:
                disease_phenos = self._disease_pheno_map.get(disease, set([]))
                score = self.compute_phenotype_match(patient_phenotypes, disease_phenos)
                disease_scores.append((score, disease))
        disease_scores.sort(reverse=True)
        return disease_scores

    def rank_genes(self, patient_genes, patient_phenotypes):
        """Compute the Phrank score for each gene matching the patient phenotypes"""
        genedisease_scores = defaultdict(list)
        genedisease_phenos = defaultdict(list)
        for disease in self._disease_pheno_map:
            if self._disease_gene_map[disease] & patient_genes:
                disease_phenos = self._disease_pheno_map.get(disease, set([]))
                score = self.compute_phenotype_match(patient_phenotypes, disease_phenos)
                for gene in self._disease_gene_map[disease] & patient_genes:
                    genedisease_scores[gene].append(score)
                    genedisease_phenos[gene].append(disease_phenos)
        gene_scores = []
        for gene in genedisease_scores:
            score = max(genedisease_scores[gene])
            phenos = genedisease_phenos[gene][genedisease_scores[gene].index(max(genedisease_scores[gene]))]
            gene_scores.append((score, gene, phenos))
        gene_scores.sort(reverse=True)
        return gene_scores

    def compute_phenotype_match(self, patient_phenotypes, query_phenotypes):
        #change_to_primary, patient_genes, disease_gene_map, disease_pheno_map, child_to_parent, disease_marginal_content)
        all_patient_phenotypes = closure(patient_phenotypes, self._child_to_parent)
        all_query_phenotypes = closure(query_phenotypes, self._child_to_parent)
        similarity_score = 0
       	for phenotype in all_patient_phenotypes & all_query_phenotypes:
            if phenotype in self._marginal_IC:
                similarity_score += self._marginal_IC.get(phenotype)
        return (similarity_score)   
 
    def compute_phenotype_match_weighted(self, patient_phenotypes, query_phenotypes):
        #change_to_primary, patient_genes, disease_gene_map, disease_pheno_map, child_to_parent, disease_marginal_content)
        all_patient_phenotypes = closure(patient_phenotypes, self._child_to_parent)
        all_query_phenotypes = closure(query_phenotypes, self._child_to_parent)
        similarity_score = 0
	#size = float(0.0) 
        total = 0
        for phenotype in all_patient_phenotypes | all_query_phenotypes:
            if not self._marginal_IC.get(phenotype):
                continue
            if phenotype in all_patient_phenotypes & all_query_phenotypes:
                similarity_score += self._marginal_IC.get(phenotype)
            total += self._marginal_IC.get(phenotype)
    	    #size += 1 
        if total > 0:
            return (similarity_score/ total)
        else:
            return similarity_score








