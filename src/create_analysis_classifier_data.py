#! /usr/bin/env python3

import sys
import os
from collections import namedtuple
from collections import defaultdict
import pickle
import logging

from util import tail, read_tsv
import __settings__
import create_reanalysis_classifier_data as r
from patients.phenotyper import get_phenotyper

handler = logging.StreamHandler(sys.stderr)
logger = logging.getLogger(__file__)
logger.addHandler(handler)
level = logging.getLevelName('DEBUG')
logger.setLevel(level)


class EntryList:

    _entries = []

    def __init__(self, entries):
        self._entries = entries

    def __iter__(self):
        return iter(self._entries)

    def __len__(self):
        return len(self._entries)


feature_map = defaultdict(lambda: [])


def write_feature(feature_name, value):
    feature_map[feature_name].append(value)


def flush_features( patient_id, features_path ):
    for feature_name in feature_map:
        out_filename = os.path.join(features_path, patient_id + '.' + feature_name + '.pkl') ##
        pickle.dump(feature_map[feature_name], open(out_filename, "wb"))

def change_val( v ):
    if v < 0:
        return -1
    return v

def write_features(entry):

    average_mcap = r.get_average_mcap(entry)
    write_feature('average_mcap', average_mcap)

    mcap_100bp = r.get_mcap_100bp(entry)
    write_feature('mcap_100bp', mcap_100bp)

    gene_mcap = r.get_gene_mcap(entry)
    write_feature('gene_mcap', gene_mcap)

    pli = entry.gene_pli_score
    write_feature('gene_pli', pli)

    gene_rvis_score = entry.gene_rvis_score
    write_feature('gene_rvis', gene_rvis_score)
    write_feature("average_allele_count", entry.average_allele_count)
    write_feature("n_paralog_by_disease", change_val(entry.n_paralog_by_disease))
    write_feature("n_ortholog_by_gene_only_hp", change_val(entry.n_ortholog_by_gene_only_hp))
    write_feature("n_betw_paralog_by_gene_only_hp", change_val(entry.n_betw_paralog_by_gene_only_hp))
    #write_feature("n_betw_paralog_by_gene", change_val(entry.n_betw_paralog_by_gene))
    write_feature("n_interaction_filtered_by_disease", change_val(entry.n_interaction_filtered_by_disease))
    write_feature("n_interaction_filtered_by_gene", change_val(entry.n_interaction_filtered_by_gene))
    #write_feature("n_interaction_filtered_pathway_neighbors_by_gene", change_val(entry.n_interaction_filtered_pathway_neighbors_by_gene))
    #write_feature("n_interaction_filtered_pathway_neighbors_by_disease", change_val(entry.n_interaction_filtered_pathway_neighbors_by_disease))
    write_feature("num_variants_in_gene", entry.num_variants_in_gene)
    write_feature("n_ortholog_by_gene_zebrafish_only_hp", change_val(entry.n_ortholog_by_gene_zebrafish_only_hp))
    write_feature("n_betw_paralog_by_gene_zebrafish_only_hp", change_val(entry.n_betw_paralog_by_gene_zebrafish_only_hp))
    #write_feature("n_betw_paralog_by_gene_zebrafish", change_val(entry.n_betw_paralog_by_gene_zebrafish))
    write_feature('n_interaction_filtered_neighbor_num_cand', entry.n_interaction_filtered_neighbor_num_cand )
    write_feature('n_interaction_filtered_2_neighbor_num_cand', entry.n_interaction_filtered_2_neighbor_num_cand )	
    write_feature('n_pathway_by_gene', change_val(entry.n_pathway_by_gene))

def featurize(patient_id, entries, features_path ):
    index_to_entry = []
    for entry in entries:
        write_features(entry)
        index_to_entry.append(entry)
    flush_features(patient_id=patient_id, features_path=features_path )
    patient_index_to_entry_output = os.path.join(features_path, patient_id + '.index_to_entry.pkl')
    pickle.dump(index_to_entry, open(patient_index_to_entry_output, "wb"))


SolutionEntry = namedtuple("SolutionEntry", ["patient", "solution"])



def parse_solution(line):
    line = line.rstrip().split('\t')
    patient, solution = line[:2]
    return SolutionEntry(patient=patient, solution=solution)

def get_patient_num_genes(filename):
    logger.info("Getting number of patient genes from %s" % filename)
    candidate_genes = set()
    for row in read_tsv(filename):
        assert row[7].startswith('ENSG'), row[7]
        candidate_genes.add(row[7])
    return len(candidate_genes)


def main():
    traces_dir = sys.argv[1]
    patient_id = sys.argv[2]
    features_path = sys.argv[3]
    patient_input = os.path.join(traces_dir, patient_id + ".trace.txt" ) ##
    logger.info("Taking input from %s" % patient_input)
    entries = EntryList([r.make_entry(line) for line in tail(patient_input)])
    featurize(patient_id=patient_id, entries=entries, features_path=features_path)

    

if __name__ == "__main__":
    logger.info("Starting")
    main()
