#! /usr/bin/env python3

import sys

in_field = sys.argv[1]
out_field = sys.argv[2]
print("\t".join(["#Chr", "Start", "End", "Ref", "Alt", out_field]))
for line in sys.stdin:
    if line.startswith('#'):
        continue
    annovar_chrom, annovar_start, annovar_end, annovar_ref, annovar_alt, chrom, pos, id, ref, alts, qual, filter, info = line.strip().split('\t')
    alcts = info.lstrip('%s=' % in_field).split(',')
    alcts = { int(x.split(':')[0]): x.split(':')[1] for x in alcts }
    translated_alts = []
    for alt in alts.split(','):
        if ref.startswith(alt):
            assert len(alt) == 1, alt
            ref_to_print = ref[len(alt):]
            alt_to_print = "-"
        elif alt.startswith(ref):
            assert len(ref) == 1, ref
            alt_to_print = alt[len(ref):]
            ref_to_print = "-"
        elif alt == "<INV>" or alt == "<DEL>":
            alt_to_print = "-"
            ref_to_print = ref
        else:
            ref_to_print = ref
            alt_to_print = alt
        assert alt_to_print != annovar_alt or (ref_to_print == annovar_ref or (annovar_ref == '0' and ref_to_print == 'N')), (ref_to_print, annovar_ref) # kinda weak assertion, but fairly confident it'll work
        translated_alts.append(alt_to_print)
    assert len(set(translated_alts)) == len(translated_alts), 'annovar_alt: %s, translated_alts: %s, line: %s' % (str(annovar_alt), str(translated_alts), line)
    assert annovar_alt in translated_alts, 'annovar_alt: %s, translated_alts: %s, line: %s' % (str(annovar_alt), str(translated_alts), line)
    index_of_alt = translated_alts.index(annovar_alt) + 1
    alct_to_print = alcts[index_of_alt]
    if float(alct_to_print) > 0:
        print("\t".join(str(x) for x in [annovar_chrom, annovar_start, annovar_end, annovar_ref, annovar_alt, alct_to_print]))
