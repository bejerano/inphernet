import sys
from net_info_extractor import Net_info_extractor
import pickle 
import os
import gc
import threading


global NEG_VAL
NEG_VAL = -1


def get_patients( p_id, pheno ):
    gene = set()
    for j in open( p_id, 'r' ):
        if j.startswith('#'):
            continue
        j = j.rstrip().split('\t')
        if float(j[7]) > 0.005 or float(j[10]) > 0.005:
            continue
        if j[11] != "exonic" and j[11] != "splicing":
            continue
        for k in j[12].split(';'):
            gene.add(k)
    return_list = []
    return_list.append(len(gene))
    return_list.append(len(gene))	
    return_list.append('N/A')
    return_list.append('N/A')
    return_list.append('N/A')
    return_list.append('N/A')
    return_list.append('N/A')
    return_list.append('N/A')
    return_list.append(list(gene))
    return_list.append([x.strip() for x in open(pheno, 'r').readlines()[0].split('\t')[-1].replace('{','').replace('}', '').split(',')])
    return return_list


def add_pheno( gene, preset, homologs, tool ): 
	return_set = set()
	return_set.update(preset)
	if gene in homologs:
		for i in homologs[gene]:
			return_set.update(tool.get_pheno(i))
	return return_set

def all_pheno( genes, tool ):
	return_set = set()
	for i in genes:
		return_set.update(tool.get_pheno(i))
	return return_set


def best_pheno( candidates, tool, remove, stack, model=False ):
    return_val = -1 
    best = ""
    count = 1
    for i in candidates.difference(set(remove)):
        count +=1
        if i in stack:
            val = stack[i]
        else:
            if model:
                val = tool.get_pheno_to_patient_phrank_normalized(tool.get_human_pheno_only(tool.get_pheno(i)))
            else:
                val = tool.get_pheno_to_patient_phrank_normalized(tool.upheno_phrank._disease_pheno_map[i])
            stack[i] = val
        if val > return_val:
            return_val = val
            best = i
    return return_val, best

def best_phrank( disease, tool, stack ):
    return_val = -1
    best = ""
    for i in disease:
        if i not in stack:
            phenos = tool.disease_pheno( i )
            if not phenos:
                val = -1
            else:
                val = tool.get_pheno_to_patient_phrank_normalized(phenos)
            stack[i] = val
        else:
            val = stack[i]
        if val > return_val:
            return_val = val
            best = i
    return return_val, best

def get_gene_names( g, t ):
    if type(g) == list or type(g) == set:
        to_p = ""
        for i in g:
            name = t.convert_from_id(i, t.gene_ref, "org")
            if '[' in name:
                name = name.replace('[','').replace(']','').split(', ')[0]
                to_p += name + '*'
        return to_p[:-1]
    else:
        name = t.convert_from_id(g, t.gene_ref, "org")
        if name:
            if '[' in name:
                return name.replace('[','').replace(']','').split(', ')[0]
            return name
        else:
            return "N/A"

def by_disease( gene_set, t, remove, stack, model=False ):
    diseases = set()
    for i in gene_set:
        diseases = diseases.union(t.get_mondo(i))
    if diseases:
        [val, disease] = best_phrank(diseases, t, stack['d'])
        g = [x for x in gene_set if disease in t.get_mondo(x)]
        return val, get_gene_names( g[0], t )
    else:
        return -1, "N/A"

def by_gene( gene_set, t, remove, stack, convert=True, model=False ):
    [v, b] = best_pheno( gene_set, t, remove, stack['g'], model )
    if convert:
        return v, get_gene_names( b, t )
    else:
        return v, b

def run_analysis( gene, tool, val, result, name, cand, stack ):
    to_return = {}
    neg_val = -1
    to_return['Phrank'] = val
    #1, 2	
    ## Paralog best gene, best disease

    if gene in tool.paralog and tool.paralog[gene]:
        [v, b] = by_disease( tool.paralog[gene], tool, cand, stack )
        to_return['Paralog'] = [v, b, "NCBITaxon_9606"]
    else:
        to_return['Paralog'] = [-1, "N/A", "NCBITaxon_9606"]


    #3, 4
    ## Ortholog only human pheno
    if gene in tool.ortholog and tool.ortholog[gene]:
        [v, b] = by_gene( tool.ortholog[gene], tool, cand, stack, True, True )
        to_return['Ortholog'] = [v, b, "NCBITaxon_10090"]
    else:
        to_return['Ortholog'] = [-1, "N/A", "NCBITaxon_10090"]

    #5, 6
    ## Between species paralog, best gene
    if gene in tool.between_paralog and tool.between_paralog[gene]:
        [v, b] = by_gene( tool.between_paralog[gene], tool, cand, stack, True, True )
        to_return['Between species paralog'] = [v, b, "NCBITaxon_10090"]
    else:
        to_return['Between species paralog'] = [-1, "N/A", "NCBITaxon_10090"]

    #7, 8
    ## Gene interactions, filtered, best disease
    if gene in tool.uberon_gene_interactions and tool.uberon_gene_interactions[gene]:
        [v, b] = by_disease( tool.uberon_gene_interactions[gene], tool, cand, stack )
        to_return['Interaction_disease'] = [v, b, "NCBITaxon_9606"]
    else:
        to_return['Interaction_disease'] = [-1, "N/A", "NCBITaxon_9606"]

    #9, 10
    ## Gene interactions, filtered, best gene
    if gene in tool.uberon_gene_interactions and tool.uberon_gene_interactions[gene]:
        [v, b] = by_gene( tool.uberon_gene_interactions[gene], tool, cand, stack )
        to_return['Interaction_gene'] = [v, b, "NCBITaxon_9606"]
    else:
        to_return['Interaction_gene'] = [-1, "N/A", "NCBITaxon_9606"]
    """
    #11, 12
    ## Gene interactions of same pathway, by gene
    if gene in tool.gene_pathway and tool.gene_pathway[gene]:
        all_same_path = set()
        for path in tool.gene_pathway[gene]:
            all_same_path = all_same_path.union(tool.pathway_gene[path])
        inter = all_same_path.intersection(set(tool.human_genes)) 
        [v, b] = by_gene( inter, tool, cand, stack )
        if b in tool.uberon_gene_interactions and tool.uberon_gene_interactions[b]:
            [sv, sb] = by_gene( tool.uberon_gene_interactions[b], tool, cand, stack )
            to_return['Interaction_pathway_gene'] = [sv, sb, "NCBITaxon_9606"]
        else:
            to_return['Interaction_pathway_gene'] = [-1, "N/A", "NCBITaxon_9606"]
    else:
        to_return['Interaction_pathway_gene'] = [-1, "N/A", "NCBITaxon_9606"]
    
    #13, 14
    ## Gene interactions of same pathway, by disease
    if gene in tool.gene_pathway and tool.gene_pathway[gene]:
        all_same_path = set()
        for path in tool.gene_pathway[gene]:
            all_same_path = all_same_path.union(tool.pathway_gene[path])
        inter = all_same_path.intersection(set(tool.human_genes)) 
        [v, b] = by_disease( inter, tool, cand, stack )
        if b in tool.uberon_gene_interactions and tool.uberon_gene_interactions[b]:
            [sv, sb] = by_disease( tool.uberon_gene_interactions[b], tool, cand, stack )
            to_return['Interaction_pathway_disease'] = [sv, sb, "NCBITaxon_9606"]
        else:
            to_return['Interaction_pathway_disease'] = [-1, "N/A", "NCBITaxon_9606"]
    else:
        to_return['Interaction_pathway_disease'] = [-1, "N/A", "NCBITaxon_9606"]
    """

    #15, 16
    ## Ortholog zebrafish only human pheno
    if gene in tool.ortholog_zebrafish and tool.ortholog_zebrafish[gene]:
        [v, b] = by_gene( tool.ortholog_zebrafish[gene], tool, cand, stack, True, True )
        to_return['Ortholog_zebrafish'] = [v, b, "NCBITaxon_7955"]
    else:
        to_return['Ortholog_zebrafish'] = [-1, "N/A", "NCBITaxon_7955"]

    #17, 18
    ## Between species paralog zebrafish, best gene, only human
    if gene in tool.between_paralog_zebrafish and tool.between_paralog_zebrafish[gene]:
        [v, b] = by_gene( tool.between_paralog_zebrafish[gene], tool, cand, stack, True, True )
        to_return['Between_species_paralog_zebrafish'] = [v, b, "NCBITaxon_7955"]
    else:
        to_return['Between_species_paralog_zebrafish'] = [-1, "N/A", "NCBITaxon_7955"]

    #19,20
    ## Same pathway
    if gene in tool.gene_pathway and tool.gene_pathway[gene]:
        curr = -1
        max_g = ""
        for path in tool.gene_pathway[gene]:
            temp = tool.pathway_gene[path].copy()
            temp.discard(gene)
            human = set([x for x in temp if x in tool.human_genes])
            temp.difference_update(human)
            [v,b] = by_gene( temp, tool, cand, stack, False, True )
            [v1, b1] = by_disease(human, tool, cand, stack )
            if v > curr and v > v1:
                max_g = tool.convert_from_id(b, tool.gene_ref, "NULL")
                curr = v
            elif v1 > curr and v1 >= v:
                max_g = "%s*NCBITaxon_9606" %(b1)
                curr = v1
        if max_g:
            to_return['Pathway'] = [curr, max_g.split('*')[0], max_g.split('*')[1]]
        else:
            to_return['Pathway'] = [curr, "N/A", "NCBITaxon_9606"]
    else:
        to_return['Pathway'] = [neg_val, "N/A", "NCBITaxon_9606"]

    #21
    ## how many of the neighbors are candidates
    if gene in tool.uberon_gene_interactions and tool.uberon_gene_interactions[gene]:
        #to_return['num_cand'] = set(tool.human_disease_genes).intersection(tool.uberon_gene_interactions[gene]) 
        to_return['num_cand'] = set(cand).intersection(tool.uberon_gene_interactions[gene]) 
    else:
        to_return['num_cand'] = neg_val

    #22
    ## how many of the 2 neighbors are candidates 
    if gene in tool.uberon_gene_interactions and tool.uberon_gene_interactions[gene]:
        total = set()
        for b in tool.uberon_gene_interactions[gene]:
            if b in tool.uberon_gene_interactions and tool.uberon_gene_interactions[b]:
                total.update( tool.uberon_gene_interactions[b] )
        total.discard(gene)
        #to_return['num_cand_2'] = set(tool.human_disease_genes).intersection(total)
        to_return['num_cand_2'] = set(cand).intersection(total)
    else:
        to_return['num_cand_2'] = neg_val 
    
    result[name] = to_return


def run( patient, t ):
    print( "STARTING" )
    curr_thread = 0
    max_thread = 20
    thread = []
    results = {}
    t.load_patient_info( patient )
    num_candidate = len(t.patient_candidate)
    count = 0
    stack = {'g':{}, 'd': {}}
    for i in t.patient_candidate:
        print( "Analyzing %s, %d out of %d running" %(i, count, num_candidate))
        count += 1 
        gene_name = t.convert_from_id(i, t.gene_ref, "org")
        val = -1
        #disease = t.get_mondo()
        #if disease:
        #    [val, best_d] = best_phrank(disease, t)
        if curr_thread < max_thread:
            curr_thread += 1
        else:
            for tt in thread:
                tt.join()
            curr_thread = 1
            del thread
            thread = []
        tt = threading.Thread( target=run_analysis, args=( i, t, val, results, gene_name, t.patient_candidate, stack ))
        thread.append(tt)	
        tt.start()
    for tt in thread:
        tt.join()
    del thread
    return results


def main():
    variant = sys.argv[1]
    phenotype = sys.argv[2]
    output_path = sys.argv[3]
    patient_info = get_patients( variant, phenotype )
    patient_info[0] = ''
    patient_info[-1] = [x.replace(':','_') for x in patient_info[-1]] 
    t = Net_info_extractor()
    for i in range(len(patient_info)):
        if type(patient_info[i]) == list:
            for j in range(len(patient_info[i])):
                patient_info[i][j] = str(patient_info[i][j])
        else:
            patient_info[i] = str(patient_info[i])
    #t.uberon_gene_interactions_2 = t.gene_interactions
    output = run( patient_info, t )
    pickle.dump( output, open( os.path.join( output_path, "%s.pkl" %( variant.split('/')[-1].split('.')[0])), 'wb'  ))    
		


if __name__ == "__main__":
	main()





