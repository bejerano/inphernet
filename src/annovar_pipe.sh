#! /bin/zsh -ex

file=$1
file_basename=$2
output_basedir=$(pwd)/$3
script_basedir=$4


echo "Have file ${file}, output dir ${output_basedir}"
# annovar should have capabilities to only keep non-0/0 genotypes and keep the genotype information possibly
cat ${file} | awk -F $'\t' '$0 ~ /^#.*/ || !($10 ~ /\.\/\./ || $10 ~ /0\/0/)' > ${output_basedir}/${file_basename}_remove.vcf
${script_basedir}/convert2annovar.pl -format vcf4 ${output_basedir}/${file_basename}_remove.vcf > ${output_basedir}/${file_basename}_remove2.avinput
${script_basedir}/table_annovar.pl ${output_basedir}/${file_basename}_remove2.avinput src/build_patient_variants/annovar_pipe/hg19 -buildver hg19 -protocol gene,ensGene -operation g,g -nastring . -out ${output_basedir}/${file_basename}_remove3 -otherinfo
tail -n+2 ${output_basedir}/${file_basename}_remove3.hg19_multianno.txt | awk -F $'\t' '($6 ~ /exonic/ || $6 ~ /splicing/) && $6 != "ncRNA_exonic" && $9 != "synonymous SNV" && $6 != "ncRNA_splicing" {OFS="\t"; print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $14}' > ${output_basedir}/${file_basename}_remove4.avinput
${script_basedir}/table_annovar.pl ${output_basedir}/${file_basename}_remove4.avinput src/build_patient_variants/annovar_pipe/hg19 -buildver hg19 -protocol exac03 -operation f -nastring 0.0 -out ${output_basedir}/${file_basename}_remove5 -otherinfo
cat ${output_basedir}/${file_basename}_remove5.hg19_multianno.txt | awk -F $'\t' '$6 <= 0.03' | cut -f 1-10,12,14- > ${output_basedir}/${file_basename}_remove6.avinput
${script_basedir}/table_annovar.pl ${output_basedir}/${file_basename}_remove6.avinput src/build_patient_variants/annovar_pipe/hg19 -buildver hg19 -protocol exac03_hmct,exac03_alct,kgp_alfq,kgp_alct,kgp_hmct -operation f,f,f,f,f -nastring 0 -out ${output_basedir}/${file_basename}_remove7 -otherinfo
cat <(echo "#Chrom\tStart\tEnd\tRef\tAlt\tExAC_HMCT\tExAC_ALCT\tKGP_ALFQ\tKGP_ALCT\tKGP_HMCT\tExAC_ALFQ\tGenePosition\tGene\tExonicFunc\tAAChange\tEnsemblID\tZygosity") <(tail -n+2 ${output_basedir}/${file_basename}_remove7.hg19_multianno.txt) > ${output_basedir}/${file_basename}.variants.txt


rm -f ${output_basedir}/${file_basename}_remove.vcf
rm -f ${output_basedir}/${file_basename}_remove2.*
rm -f ${output_basedir}/${file_basename}_remove3.*
rm -f ${output_basedir}/${file_basename}_remove4.*
rm -f ${output_basedir}/${file_basename}_remove5.*
rm -f ${output_basedir}/${file_basename}_remove6.*
rm -f ${output_basedir}/${file_basename}_remove7.*




