#! /usr/bin/env python3
# -*- coding: ascii -*-

from __future__ import print_function
import logging
import sys

from collections import defaultdict, namedtuple
import os
import sqlite3 as lite
import gzip
import pickle

from functools import cmp_to_key

from patients import patient_info
import __settings__
import threading
#from text import text_database
from patients.phenotyper import get_phenotyper
from text.gene_db import EidToNameDB
from util import read_tsv

from variants.seq_data import SeqData

handler = logging.StreamHandler(sys.stderr)
logger = logging.getLogger(__file__)
logger.addHandler(handler)
level = logging.getLevelName('DEBUG')
logger.setLevel(level)
sys.stderr.flush()


Result = namedtuple('Result', [
                               'gene_name',
                               'eid',
                               'phenotypes',
                               'gene_rvis_score',
                               'average_mcap',
                               'mcap_100bp',
                               'gene_mcap',
                               'gene_pli_score',
                               'average_allele_count',
			       'n_paralog_by_disease',
			       'n_ortholog_by_gene_only_hp',
			       'n_betw_paralog_by_gene_only_hp',
		               'n_interaction_filtered_by_disease',
			       'n_interaction_filtered_by_gene',
			       'num_variants_in_gene',
			       'n_ortholog_by_gene_zebrafish_only_hp',
			       'n_betw_paralog_by_gene_zebrafish_only_hp',
			       'n_interaction_filtered_neighbor_num_cand',
			       'n_interaction_filtered_2_neighbor_num_cand',
                               'n_pathway_by_gene'
			 ])

gene_name_to_rvis = None


def rvis_score(gene_name):
	global gene_name_to_rvis
	if gene_name_to_rvis is None:
		gene_name_to_rvis = {}
		with open(os.path.join(__settings__.APP_HOME, 'data', 'onto', 'genes', 'gene_name_to_rvis.tsv')) as f:
			for line in f:
				line = line.rstrip('\n').split('\t')
				gn = line[0]
				score = float(line[1])
				gene_name_to_rvis[gn] = score
	if gene_name in gene_name_to_rvis:
		return gene_name_to_rvis[gene_name]
	else:
		return 100.0

eid_to_pli = None


def load_eid_to_pli():
	global eid_to_pli
	eid_to_pli = {}
	with gzip.open(os.path.join(__settings__.APP_HOME, 'data' , 'onto', 'genes', 'exacintolerance.txt.gz'), mode='rt') as f:
		for line in f.readlines():
			line = str(line)
			if line.startswith('#'):
				continue
			line = line.rstrip('\n').split('\t')
			eid = line[0]
			pli = float(line[1].split(':')[-1])
			eid_to_pli[eid] = pli


def pli_score(eid):
	global eid_to_pli
	if not eid_to_pli:
		load_eid_to_pli()
	if eid not in eid_to_pli:
		return 0.0
	return eid_to_pli[eid]

eid_to_average_mcap = None


def load_eid_to_average_mcap():
    global eid_to_average_mcap
    eid_to_average_mcap = {}
    with open(os.path.join(__settings__.APP_HOME, 'data' , 'onto', 'genes', 'ensg_to_mcap_stats.tsv')) as f:
        for line in f:
            line = line.rstrip('\n').split('\t')
            eid = line[0]
            mcap = float(line[2])
            eid_to_average_mcap[eid] = mcap


#CHANGED TO MAX
def get_gene_mcap(eid, patient_genes):
    mcaps = [v.mcap for v in patient_genes[eid].variants if v.mcap is not None]
    if len(mcaps) > 0:
        return max(mcaps)
    else:
        return None


    if not eid_to_average_mcap:
        load_eid_to_average_mcap()
    if eid not in eid_to_average_mcap:
        return None
    return eid_to_average_mcap[eid]

#CHANGED TO MAX
def get_variants_average_mcap(eid, patient_genes):
    mcaps = [v.mcap for v in patient_genes[eid].variants if v.mcap is not None]
    if len(mcaps) > 0:
        #rv = max(mcaps) 
        rv = sum(mcaps) / len(mcaps)
    else:
        rv = None
    return rv

#CHANGED TO MAX
def get_mcap_100bp(eid, patient_genes):
    mcap_100bps = [v.mcap_100bp for v in patient_genes[eid].variants if v.mcap_100bp is not None]
    if len(mcap_100bps) > 0:
        rv = max(mcap_100bps)
        #rv = sum(mcap_100bps) / len(mcap_100bps)
    else:
        rv = None
    return rv


def get_variant_types(eid, patient_genes):
	return [v.get_semantic_effect() for v in patient_genes[eid].variants]


def get_variant_coords(eid, patient_genes):
	return ["chr" + v.chrom + ":" + str(v.start) + "-" + str(v.end) for v in patient_genes[eid].variants]





def load_eid_to_refseq():
	global eid_to_refseq
	rv = defaultdict(lambda: [])
	for row in read_tsv(os.path.join(__settings__.APP_HOME, "data/onto/variants/ensembl_to_refseq.tsv")):
		rv[row[0]].append(row[1])
	eid_to_refseq = rv

variants_conn = None
variants_cur = None


def load_variants_conn(date_x, date_y):
	global variants_conn, variants_cur
	variants_conn = lite.connect(os.path.join(__settings__.APP_HOME, "data",
				"onto",
				"genes",
				"classified_variants_Xall_Yall.hg19.sqlite3" ))
	variants_cur = variants_conn.cursor()

ExtractedVariant = namedtuple('ExtractedVariant', ['pmid', 'chrom', 'start', 'end',
			'gene_name', 'checked', 'variant_type',
			'length', 'strand', 'orig_str',
			'vcf_pos', 'vcf_ref', 'vcf_alt',
			'refseq_id', 'start_codon', 'exons'])


#@profile
def get_extracted_variants(pmid, ensembl_id):
	if (pmid, ensembl_id) in get_extracted_variants.cache:
		return get_extracted_variants.cache[pmid, ensembl_id]
	rv = []
	variants_cur.execute('SELECT * FROM variants WHERE pmid=? and ensembl_id like ?', [pmid, "%" + ensembl_id + "%"])
	rows = variants_cur.fetchall()
	for row in rows:
		variant_pmid, chrom, start, end, gene_name, ensembl_id, entrez_id, checked, variant_type, \
		refseqprot_id, length, strand, orig_str, vcf_pos, vcf_ref, vcf_alt, refseq_id, \
		start_codon, region, classification_score, classification_comment = row
		rv.append(ExtractedVariant(pmid=pmid, chrom=chrom, start=int(start), end=int(end), gene_name=gene_name,
					checked=checked, variant_type=variant_type, length=length,
					strand=strand, orig_str=orig_str, vcf_pos=vcf_pos,
					vcf_ref=vcf_ref, vcf_alt=vcf_alt, refseq_id=refseq_id.split('.')[0],
					start_codon=int(start_codon), exons=set(region.split(','))))
	get_extracted_variants.cache[pmid, ensembl_id] = rv
	return rv

get_extracted_variants.cache = {}


def num_variants_in_gene(ensembl_id):
    variants_cur.execute('SELECT count(*) FROM (SELECT DISTINCT chrom, start, end FROM ' 'variants WHERE ensembl_id like ?)', ["%" + ensembl_id + "%"])
    row = variants_cur.fetchone()
    return row[0]



eid_to_refseq = None


def convert_to_empty( s, val ):
    s = float(s[0]) if type(s) == list else float(s)
    if s < 0:
        return val
    return s

def convert_to_length( s ):
    if type(s) is set:
        return len(s)
    return s

def get_average_allele_count(eid, patient_genes):
	allele_counts = []
	for v in patient_genes[eid].variants:
		allele_counts.append(v.exac_alct)
	return float(sum(allele_counts)) / len(allele_counts)


	
def solve( eid, gene_name, patient_genes, patient_hpo_ids, date_x, date_y, network_prop, num_variant, output):
    overlap_list = []
    sys.stderr.flush()
    overlap_scores = {}
    paper_maxs = {}    
    empty_val = -1 ## MODIGY FOR EMPTY VALUE

    if gene_name not in network_prop:
        return None

#change SOLVER2
    result = Result(
        gene_name=gene_name,
        eid=eid,
        phenotypes=patient_hpo_ids,
        gene_rvis_score=rvis_score(gene_name),
        average_mcap=get_variants_average_mcap(eid, patient_genes),
        mcap_100bp=get_mcap_100bp(eid, patient_genes),
        gene_mcap=get_gene_mcap(eid, patient_genes),
        gene_pli_score=pli_score(eid),
        average_allele_count=get_average_allele_count(eid, patient_genes),
        n_paralog_by_disease=convert_to_empty(network_prop[gene_name]['Paralog'], empty_val),
        n_ortholog_by_gene_only_hp=convert_to_empty(network_prop[gene_name]['Ortholog'], empty_val),
        n_betw_paralog_by_gene_only_hp=convert_to_empty(network_prop[gene_name]['Between species paralog'], empty_val),
        n_interaction_filtered_by_disease=convert_to_empty(network_prop[gene_name]['Interaction_disease'], empty_val),
        n_interaction_filtered_by_gene=convert_to_empty(network_prop[gene_name]['Interaction_gene'], empty_val),
        n_ortholog_by_gene_zebrafish_only_hp=convert_to_empty(network_prop[gene_name]['Ortholog_zebrafish'], empty_val),
        n_betw_paralog_by_gene_zebrafish_only_hp=convert_to_empty(network_prop[gene_name]['Between_species_paralog_zebrafish'], empty_val),
        num_variants_in_gene=num_variant,
        n_interaction_filtered_neighbor_num_cand = convert_to_empty(convert_to_length(network_prop[gene_name]['num_cand']), empty_val),
        n_interaction_filtered_2_neighbor_num_cand = convert_to_empty(convert_to_length(network_prop[gene_name]['num_cand_2']), empty_val),
        n_pathway_by_gene = convert_to_empty(network_prop[gene_name]['Pathway'], empty_val)
        )
    m = [
        ('gene_name', result.gene_name),
        ('eid', result.eid),
        ('phenotypes', ','.join(result.phenotypes)),
        ('gene_rvis_score', result.gene_rvis_score),
        ('average_mcap', str(result.average_mcap)),
        ('mcap_100bp', str(result.mcap_100bp)),
        ('gene_mcap', str(result.gene_mcap)),
        ('gene_pli_score', result.gene_pli_score),
        ('average_allele_count', result.average_allele_count),	
        ('n_paralog_by_disease', result.n_paralog_by_disease),
        ('n_ortholog_by_gene_only_hp', result.n_ortholog_by_gene_only_hp),
        ('n_betw_paralog_by_gene_only_hp', result.n_betw_paralog_by_gene_only_hp),
        ('n_interaction_filtered_by_disease', result.n_interaction_filtered_by_disease),
        ('n_interaction_filtered_by_gene', result.n_interaction_filtered_by_gene ),
        ('num_variants_in_gene', result.num_variants_in_gene),
        ('n_ortholog_by_gene_zebrafish_only_hp', result.n_ortholog_by_gene_zebrafish_only_hp),
        ('n_betw_paralog_by_gene_zebrafish_only_hp', result.n_betw_paralog_by_gene_zebrafish_only_hp),
        ('n_interaction_filtered_neighbor_num_cand', result.n_interaction_filtered_neighbor_num_cand ),
        ('n_interaction_filtered_2_neighbor_num_cand', result.n_interaction_filtered_2_neighbor_num_cand),
        ('n_pathway_by_gene', result.n_pathway_by_gene )
        ]
    line = ""
    for name, value in m:
        line +='%s=%s;' % (name, str(value))
    output.append(line[:-1])
    return
def get_gene_ref(basedir, individual):
    ref = {}
    with open(os.path.join(basedir, '%s.variants.txt' % (individual))) as f:
        for i in f:
            if i.startswith('#'):
                continue
            i = i.rstrip().split('\t')
            eid = i[16]
            gene = i[12]
            ref[eid] = gene
    return ref
        

def main():
    basedir = sys.argv[1]
    patient = sys.argv[2]
    if ',' in patient:
        patient = patient.split(',')[1]
    date_x = sys.argv[3]
    date_y = sys.argv[4]
    network_file = os.path.join(sys.argv[5],"%s.pkl" %(patient))
    network_prop = pickle.load( open( network_file, 'rb' ) )
    assert sys.argv[6] == "count" or sys.argv[6] == "no_count"
    filter_by_count = False
    alct_cutoff = int(sys.argv[7])
    hmct_cutoff = int(sys.argv[8])
    patient_pheno = sys.argv[9]
    outfile = open(sys.argv[10], 'w')
    logger.info("Loading variants connection")
    load_variants_conn(date_x, date_y)
#    logger.info("Loading EID to refseq")
#    load_eid_to_refseq()
    logger.info("Getting patient EIDs")
    patient_genes = patient_info.get_patient_eids(basedir, patient,
                                              alfq_cutoff=0.005,
                                              dominant_alfq_cutoff=0.001,
                                              filter_by_count=filter_by_count,
                                              hmct_cutoff=hmct_cutoff,
                                              alct_cutoff=alct_cutoff)
    patient_gene_ref = get_gene_ref(basedir, patient)
    logger.info(len(patient_genes))
    logger.info("Getting patient HPO IDs")
    patient_hpo_ids = patient_info.get_patient_hpo_ids( patient_pheno )
    count = 0
    threads = []
    output_thread = []
    for gene in patient_genes:
        with EidToNameDB() as db:
            gene_name = db.query(gene)
        if not gene_name and gene in patient_gene_ref:
            gene_name = patient_gene_ref[gene]
        num_variant = num_variants_in_gene(gene)
        t = threading.Thread(target= solve, args =(gene, gene_name, patient_genes, patient_hpo_ids, date_x, date_y, network_prop, num_variant, output_thread))
        t.start()
        threads.append(t)
        if len(threads) > 20:
            for t in threads:
                t.join()
                count += 1
                print("Finished featurizing %s/%s genes" %(count, len(patient_genes)))
            threads = []
    for line in output_thread:
        if not line:
            continue
        outfile.write( "%s\n" %(line) )	


if __name__ == "__main__":
    logger.info("Hello")
    sys.stderr.flush()
    main()













