
import os, sys
from os.path import join

from util import max_iter_tsv_rows


def parse_entrez(fname):
    """ parse a tab-sep table with headers and return one dict with entrez to ref_prots
    and another dict with entrez to symbol
    """
    entrez_to_sym = dict()
    entrez_to_refseq_prots = dict()
    entrez_to_refseq_coding_seqs = dict()

    for row in max_iter_tsv_rows(fname):
        entrez_to_sym[int(row.entrezId)] = row.sym
        if row.refseqProtIds == "":
            ref_prots = None
        else:
            ref_prots = row.refseqProtIds.split(",")
            # assert(len(ref_prots)==len(refseqs))
        if row.refseqIds == "":
            refseq_ids = None
        else:
            refseq_ids = row.refseqIds.split(",")

        entrez_to_refseq_prots[int(row.entrezId)] = ref_prots
        entrez_to_refseq_coding_seqs[int(row.entrezId)] = refseq_ids
    return entrez_to_sym, entrez_to_refseq_prots, entrez_to_refseq_coding_seqs


#export NCBI_SOLVER_PUBMUNCH_DIR=/cluster/u/jbirgmei/shallowfloat/pubmunch
#export NCBI_SOLVER_HOME=/cluster/u/jbirgmei/shallowfloat/ncbi-solver
#export NLTK_DATA=/cluster/u/jbirgmei/nltk_data
#export NCBI_SOLVER_DATA_DIR=/cluster/u/jbirgmei/shallowfloat/text


class SeqData(object):
    # MAX HAEUSSLER
    """ functions to get sequences and map between identifiers for entrez genes,
    uniprot, refseq, etc """

    def __init__(self):
        mut_data_dir = join(os.environ.get("/cluster/u/jbirgmei/shallowfloat/pubmunch"), 'data', 'variants')
        gene_data_dir = join(os.environ.get("/cluster/u/jbirgmei/shallowfloat/pubmunch"), 'data', 'genes')
        self.entrez_to_sym, self.entrez2ref_prots, self.entrez2refseqs = parse_entrez(join(gene_data_dir, "entrez.tab"))
        fname = join(mut_data_dir, "refseqInfo.tab")
        self.refProtToRefSeq = {}
        self.refSeqToRefProt = {}
        self.refSeqCds = {}
        for row in max_iter_tsv_rows(fname):
            refprot_id = row.refProt.split('.')[0]
            refseq_ids = row.refSeq.split('.')[0]
            self.refProtToRefSeq[refprot_id] = refseq_ids
            self.refSeqToRefProt[refseq_ids] = refprot_id
            # print("adding refseq_ids %s to cdsStart %s" % (refseq_ids, row.cdsStart), file=sys.stderr)
            self.refSeqCds[refseq_ids] = int(row.cdsStart) - 1  # NCBI is 1-based

        # refseq to genome
        self.intronChrStartEndStrands = {}
        for row in max_iter_tsv_rows(join(mut_data_dir, "refGeneIntrons.hg19.bed"), noHeaderCount=6):
            chrom = str(row.col0)
            start = int(row.col1)
            end = int(row.col2)
            names = [str(x) for x in row.col3.split('_')]
            refseq_ids = names[0] + '_' + names[1]
            strand = row.col5
            if refseq_ids not in self.intronChrStartEndStrands:
                self.intronChrStartEndStrands[refseq_ids] = []
            self.intronChrStartEndStrands[refseq_ids].append((chrom, start, end, strand))
        for refseq_ids in self.intronChrStartEndStrands:
            l = self.intronChrStartEndStrands[refseq_ids]
            l = sorted(l, key=lambda y: y[1])
            if l[0][3] == "+":
                pass
            elif l[0][3] == "-":
                l = l[::-1]
            else:
                assert False, l[0][3]
            self.intronChrStartEndStrands[refseq_ids] = l

        self.exonStartEnds = {}
        self.exonStrands = {}
        for row in max_iter_tsv_rows(join(mut_data_dir, "refGeneExons.hg19.bed"), noHeaderCount=6):
            start = int(row.col1)
            end = int(row.col2)
            names = [str(x) for x in row.col3.split('_')]
            refseq_ids = names[0] + '_' + names[1]
            # print("Adding refseq_ids %s" % refseq_ids, file=sys.stderr)
            strand = row.col5
            if refseq_ids not in self.exonStartEnds:
                self.exonStartEnds[refseq_ids] = []
            self.exonStartEnds[refseq_ids].append((start, end))
            self.exonStrands[refseq_ids] = strand
        for refseq_ids in set([x for x in self.exonStartEnds.keys()]):
            strand = self.exonStrands[refseq_ids]
            l = self.exonStartEnds[refseq_ids]
            l = sorted(l, key=lambda y: y[0])
            if strand == "+":
                pass
            elif strand == "-":
                l = l[::-1]
            else:
                assert False, strand
            self.exonStartEnds[refseq_ids] = l

    def get_cds_start(self, refseq_ids):
        """ return refseq CDS start position """
        if refseq_ids not in self.refSeqCds:
            return None
        cds_start = self.refSeqCds[refseq_ids]
        return cds_start
