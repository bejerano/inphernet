
import sys
import logging
from collections import namedtuple

handler = logging.StreamHandler(sys.stderr)
logger = logging.getLogger(__file__)
logger.addHandler(handler)
level = logging.getLevelName('DEBUG')
logger.setLevel(level)

SolutionEntry = namedtuple("SolutionEntry", ["patient", "solution"])


def parse_solution(line):
    line = line.rstrip().split('\t')
    patient, solution = line[:2]
    return SolutionEntry(patient=patient, solution=solution)


