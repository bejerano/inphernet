"""Utility for reading and writing line-based text files inspired by the
similarly-named utility from the Kent source.

A linefile supports "stdin" and "stdout" as file names and auto-detects
file compression from file extension.
"""

import io
import gzip
import bz2
import sys
import builtins
import Bio.bgzf

def open(f, mode=None, encoding=None, comment_prefix="#", skip_comments=True, skip_blanks=True):
    """Open a line-based text file, with auto-detection of file compression
    from the file extension (.gz, .bz2).

    f is either a filename or an io.TextIOBase stream.
    """

    class LineFile(io.TextIOBase):
        """A linefile, which provides convenience methods for reading and writing
        line-based text files.
        """

        def __init__(self, stream, filename, comment_prefix, skip_comments, skip_blanks):
            """Stash the underlying stream used for reading/writing."""
            self._stream = stream
            self.filename = filename
            self._comment_prefix = comment_prefix
            self._skip_comments = skip_comments
            self._skip_blanks = skip_blanks
            self._last = None
            self._reuse = False

        def readline(self):
            """Read a single line from the linefile."""

            def next_line():
                if self._reuse:
                    self._reuse = False
                    self._last = self._last if (self._last is not None) else self._stream.readline()
                else:
                    self._last = self._stream.readline()

                return self._last

            def is_good_line(s):
                return not(self._skip_blanks and (s == "\n")) and \
                    not(self._skip_comments and s.startswith(self._comment_prefix))

            s = next_line()
            while s and not is_good_line(s):
                s = next_line()

            return s

        def read(self):
            """ Read the whole file and return as one string, while respecting all commons and skips """
            return ''.join(list(self))

        def reuse(self):
            """Reuse the previously read line on the next readline()."""
            self._reuse = True

        def write(self, s):
            """Write a string to the linefile."""
            self._stream.write(s)

        def close(self):
            """Close the linefile."""
            if self._stream not in [sys.stdin, sys.stdout]:
                self._stream.close()


    mode = "r" if mode is None else mode  # default mode is "r"
    if mode not in ["r", "w", "a"]:
        raise ValueError("invalid linefile mode: '%s'" % (mode))

    # open the stream with the appropriate reader
    stream = None
    if isinstance(f, str):
        filename = f
        if f == "stdin":
            stream = sys.stdin
        elif f == "stdout":
            stream = sys.stdout
        elif f == "-":
            stream = sys.stdin if mode == "r" else sys.stdout
        elif f.endswith(".gz") or f.endswith(".Z") or f.endswith(".zip"):
            stream = io.TextIOWrapper(gzip.open(f, mode, compresslevel=6), encoding=encoding)
        elif f.endswith(".bz2"):
            stream = io.TextIOWrapper(bz2.BZ2File(f, mode), encoding=encoding)
        elif f.endswith(".bgz"):
            # BioPython bgzf does not inherit from IOBase; so, it can
            # not be wrapped in io.TextIOWrapper.  So, linefile lacks
            # encoding support when reading/writing .bgz files.
            stream = Bio.bgzf.open(f, mode)
        else:
            stream = builtins.open(f, mode, encoding=encoding)
    else:
        filename = None
        stream = f

    return LineFile(stream=stream, filename=filename, comment_prefix=comment_prefix, skip_comments=skip_comments,
        skip_blanks=skip_blanks)
