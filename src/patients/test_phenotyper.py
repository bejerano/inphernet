#! /usr/bin/env python3

import phenotyper
import os
import __settings__

if __name__ == "__main__":
    pheno = phenotyper.Phenotyper(os.path.join(__settings__.APP_HOME, "onto/hpo/hpoTerms.txt"),
                                  os.path.join(__settings__.APP_HOME, "onto/hpo/hpoDag.txt"),
                                  os.path.join(__settings__.APP_HOME, "onto/hpo/hpoDagModifications.txt"))
    print(pheno.hpoTermDict('HP:0003244'))
