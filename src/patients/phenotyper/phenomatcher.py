#! /usr/bin/env python3

import sys, os
import glob
from collections import defaultdict
import math

from . import get_gene_to_hpo, dag_file

import __settings__

def load_maps(human_phenotype_map_file):
    hpo_file = open(human_phenotype_map_file)
    child_to_parent = defaultdict(list)
    parent_to_children = defaultdict(list)
    for hpo_line in hpo_file:
        hpo_tokens = hpo_line.strip().split("\t")
        child = hpo_tokens[0]
        parent = hpo_tokens[1]
        child_to_parent[child].append(parent)
        parent_to_children[parent].append(child)
    return child_to_parent, parent_to_children

def load_term_hpo(term_to_hpo_file):
    term_hpo_file = open(term_to_hpo_file)
    term_pheno_map = defaultdict(list)
    for term_line in term_hpo_file:
        term_hpo_tokens = term_line.strip().split("\t")
        term = term_hpo_tokens[0]
        hpo = term_hpo_tokens[1]
        term_pheno_map[term].append(hpo)
    term_hpo_file.close()
    return term_pheno_map

def get_all_ancestors(hpo_term, child_to_parent_map):
    ancestors = []
    term = hpo_term
    parents = child_to_parent_map.get(term)[:]
    while parents:
        parent = parents.pop()
        ancestors.append(parent)
        parents = parents + child_to_parent_map.get(parent, [])
    return ancestors

def get_all_children(hpo_term, parent_to_child_map):
    children = []
    queue = parent_to_child_map.get(hpo_term, [])
    while queue:
        child = queue.pop()
        children.append(child)
        new_children = parent_to_child_map.get(child, [])
        queue = queue + new_children
    return children

def term_closure(term, child_to_parent_map, parent_to_children_map):
    ancestors = get_all_ancestors(term, child_to_parent_map)
    children = get_all_children(term, parent_to_children_map)
    return ancestors + [term] + children

def load_information_content(gene_pheno_map, child_to_parent_map):
    information_content, marginal_content = {}, {}
    annotatedGeneCt = 0
    associated_phenos = defaultdict(set)
    for gene, phenos in gene_pheno_map.iteritems():
        annotatedGeneCt += 1
        all_ancestors = set([])
        for pheno in phenos:
            all_ancestors = all_ancestors | set(get_all_ancestors(pheno, child_to_parent_map)) | set([pheno])

        # for each ancestor increment the count since this pheno is now associated with the specified gene
        for pheno in all_ancestors:
            associated_phenos[pheno].add(gene)

    phenos = associated_phenos.keys()
    for pheno in phenos:
        information_content[pheno] = -math.log(1.0 * len(associated_phenos[pheno]) / annotatedGeneCt, 2) if len(associated_phenos[pheno]) else 0

    for pheno in phenos:
        parent_phenos = child_to_parent_map[pheno]
        parent_entropy = 0
        if len(parent_phenos) == 1:
            parent_entropy = information_content[parent_phenos[0]]
        elif len(parent_phenos) > 1:
            list_of_phenosets = [associated_phenos[parent] for parent in parent_phenos]
            parent_set = set([])
            for phenoset in list_of_phenosets:
                parent_set = parent_set & phenoset if parent_set else phenoset
            parent_entropy = -math.log(1.0 * len(parent_set) / annotatedGeneCt, 2) if len(parent_set) else 0
        marginal_content[pheno] = information_content[pheno] - parent_entropy

    return information_content, marginal_content

def pheno_closure(phenos, child_to_parent, parent_to_children):
    # for each phenotype we need to get all ancestors and then compute the sum of ICs for all nodes in the intersection
    all_phenos = set([])
    for pheno in phenos:
        # all_phenos = all_phenos | set(term_closure(pheno, child_to_parent, parent_to_children))
        all_phenos = all_phenos | set(get_all_ancestors(pheno, child_to_parent) + [pheno])
    return all_phenos

def compute_pheno_match(patient_phenos, article_phenos,
                        gene_pheno_map, child_to_parent,
                        parent_to_children, information_content):
    all_patient_phenos = pheno_closure(patient_phenos, child_to_parent, parent_to_children)
    marginal_ICs = 0
    for pheno in all_patient_phenos & pheno_closure(article_phenos, child_to_parent, parent_to_children):
        marginal_ICs += information_content.get(pheno, 0)
    return marginal_ICs

def load_entrez_to_ensembl(entrez_map_filename):
    entrez_to_gene = defaultdict(list)
    f = open(entrez_map_filename)
    for line in f:
        tokens = line.strip().split("\t")
        gene = tokens[0]
        entrez_id = tokens[1]
        entrez_to_gene[entrez_id].append(gene)
    return entrez_to_gene

class PhenoMatcher:

    def __init__(self,
                 hpo_child_to_parent_file,
                 gene_to_hpo_file):
        self.child_to_parent, self.parent_to_children = load_maps(hpo_child_to_parent_file)
        self.gene_pheno_map = load_term_hpo(gene_to_hpo_file)
        self.information_content, self.marginal_content = load_information_content(self.gene_pheno_map,
                                                                                   self.child_to_parent)

    def phenotype_score(self, article_phenos, patient_phenos):
        return compute_pheno_match(patient_phenos, article_phenos,
                                   self.gene_pheno_map, self.child_to_parent,
                                   self.parent_to_children, self.information_content)

phenotyper = None
dateXsave = None
dateYsave = None

def get_phenotyper(dateX, dateY):
    global dateXsave
    global dateYsave
    global phenotyper
    assert dateXsave is None or dateXsave == dateX
    dateXsave = dateX
    assert dateYsave is None or dateYsave == dateY
    dateYsave = dateY
    if phenotyper is not None:
        return phenotyper
    phenotyper = PhenoMatcher(dag_file, get_gene_to_hpo(dateX, dateY))
    return phenotyper

def get_hpo_phenotyper():
    global phenotyper
    if phenotyper is not None:
        return phenotyper
    phenotyper = PhenoMatcher(dag_file, os.path.join(__settings__.APP_HOME, 'onto/hpo/hpo_gene_to_hpo.tsv'))
    return phenotyper
