import os
import collections
import functools
import itertools
import math

import sqlite3 as lite

from patients import linefile
from patients import common
import __settings__

HpoTerm = collections.namedtuple("HpoTerm", ["id", "name", "synonyms", "definition"])
ShallowFloatAnnotation = collections.namedtuple("ShallowFloatAnnotation", ["hpo_id", "entity_id"])

class Phenotyper:

    @staticmethod
    def update_ontology(dagmodfile, term_to_parents, term_to_children):
        # DAG modifications
        # Modifications are either:
        #     adds: "+ child parent", which adds an edge to the DAG
        #     removes: "- child parent", which removes an edge (or edges) from the DAG
        # DAG remove statements support the wildcard "*" for child and/or parent.
        # DAG modifications are applied in the listed order.
        f = linefile.open(dagmodfile)
        for l in f:
            (op, child, parent) = l.rstrip("\n").split("\t")

            if op == "+":
                # Add a child->parent edge
                term_to_parents[child].add(parent)
                term_to_children[parent].add(child)
            elif op == "-":
                # Remove a child->parent edge or edges.
                if child == "*" and parent == "*":
                    # Empty the DAG
                    term_to_parents.clear()
                    term_to_children.clear()
                elif child == "*":
                    # Remove all links from the specified parent to its children.
                    for c in term_to_children[parent]:
                        term_to_parents[c].remove(parent)
                    term_to_children[parent].clear()
                elif parent == "*":
                    # Remove all links from the specified child to its parents.
                    for p in term_to_parents[child]:
                        term_to_children[p].remove(child)
                    term_to_parents[child].clear()
                else:
                    # Remove a single link, if it exists.
                    if child in term_to_children[parent]:
                        term_to_children[parent].remove(child)
                        term_to_parents[child].remove(parent)
        f.close()
        return term_to_parents, term_to_children

    def __init__(self, termsfile, dagfile, entity_to_hpo_annotations):
        """Initialize the phenotyper with HPO terms, DAG, and term - entity."""

        # terms
        self._hpoTerms = {}
        f = linefile.open(termsfile)
        for l in f:
            (term_id, term_name, term_synonyms, term_defn) = l.rstrip("\n").split("\t")
            self._hpoTerms[term_id] = HpoTerm(term_id, term_name, term_synonyms, term_defn)
        f.close()

        # bottom-up DAG (child to parents) and top-down DAGs (parent to children)
        self._buHpoDag, self._tdHpoDag = common.load_map(dagfile) if dagfile else ({}, {})

        self._shallowFloatEntityToTerm = collections.defaultdict(dict)
        self._shallowFloatTermToEntity = collections.defaultdict(dict)
        for entity_id, hpo_term_id in entity_to_hpo_annotations:
                annotation = ShallowFloatAnnotation(hpo_id=hpo_term_id, entity_id=entity_id)
                self._shallowFloatEntityToTerm[entity_id][hpo_term_id] = annotation
                self._shallowFloatTermToEntity[hpo_term_id][entity_id] = annotation
        Phenotyper.canonicalize_annotations(self._buHpoDag, 
                                            self._shallowFloatEntityToTerm,
                                            self._shallowFloatTermToEntity)
        self._termMarginalIc = Phenotyper.compute_term_info_content(self._hpoTerms, self._buHpoDag,
                                                                    self._shallowFloatEntityToTerm,
                                                                    self._shallowFloatTermToEntity)

        # Define body system indexes
        self._ixToSystem = [s for s, t in bodySystemTerms.items()]
        self._otherIx = len(self._ixToSystem) - 1

        # Memoize _termToSystemIx for efficiency
        self._mzTermToSystemIx = dict()
        # Start by adding base cases.
        for ix, (system, terms) in enumerate(bodySystemTerms.items()):
            for term in terms:
                self._mzTermToSystemIx[term] = ix

    @staticmethod
    def compute_term_info_content(hpoterms, term_to_parents, entity_to_hpo, hpo_to_entity):
        # entity can be a gene, article or disease for example (anything that's 
        # associated with a pheno and can be used for info content calculation)
        annotatedEntityCt = len(entity_to_hpo)
        termRawIc = {}
        for term in hpoterms.values():
            entityAnnotations = hpo_to_entity.get(term.id, [])
            termRawIc[term.id] = -math.log2(len(entityAnnotations) / annotatedEntityCt) if len(entityAnnotations) else 0

        termMarginalIc = {}
        for term in hpoterms.values():
            if term.id not in hpo_to_entity:
                termMarginalIc[term.id] = 0
            else:
                parentTerms = term_to_parents.get(term.id, [])
                if len(parentTerms) == 0:
                    parentIc = 0
                else:
                    parentEntitySets = [set([a.entity_id for g, a in hpo_to_entity[t].items()]) for t in parentTerms]
                    parentIntersection = functools.reduce(lambda a, b: a.intersection(b), parentEntitySets)
                    parentIc = -math.log2(len(parentIntersection) / annotatedEntityCt)
                termMarginalIc[term.id] = termRawIc[term.id] - parentIc
        return termMarginalIc

    @staticmethod
    def canonicalize_annotations(term_to_parents, entity_to_hpo, hpo_to_entity):
        for entity_id in entity_to_hpo:
            direct_terms = set([a.hpo_id for a in entity_to_hpo[entity_id].values()])
            closure = Phenotyper.term_closure(direct_terms, term_to_parents)

            for term_id in closure.difference(direct_terms):
                annotation = ShallowFloatAnnotation(hpo_id=term_id, entity_id=entity_id)
                entity_to_hpo[entity_id][term_id] = annotation
                hpo_to_entity[term_id][entity_id] = annotation
        return entity_to_hpo, hpo_to_entity

    @staticmethod
    def term_closure(term_list, term_to_relatives):
        """Take the closure over a list of terms.  The closure is defined as the
        terms in the list and all its ancestors or descendants depending on the dag passed."""
        closure = set()

        def helper(term):
            # avoid retraversing a subtree
            if term not in closure:
                closure.add(term)
                for parent in term_to_relatives.get(term, set()):
                    helper(parent)

        # Traverse starting with each term in the term list
        for term in term_list:
            helper(term)
        return closure

    def term_ancestor_closure(self, term_list):
        """Take the closure over a list of terms.  The closure is defined as the
        terms in the list and all ancestors up to the root of the DAG."""
        return Phenotyper.term_closure(term_list, self._buHpoDag)

    def term_descendant_closure(self, term_list):
        """Identify all of the terms in term list or its descendants down
        to the leaves in the DAG."""
        return Phenotyper.term_closure(term_list, self._tdHpoDag)

    def _termToSystemIx(self, hpoTerm):
        """A helper for termToSystem that returns the index of the system
        into which to organize a term."""
        if hpoTerm not in self._mzTermToSystemIx:
            parents = self._buHpoDag.get(hpoTerm, [])
            self._mzTermToSystemIx[hpoTerm] = min([self._otherIx] + [self._termToSystemIx(x) for x in parents])
        return self._mzTermToSystemIx[hpoTerm]

    def termToSystem(self, hpoTerm):
        """Determine the body system into which to organize a term."""
        return self._ixToSystem[self._termToSystemIx(hpoTerm)]

    def hpoTermDict(self, hpoId):
        """Return a dictionary that lists details about an HPO term."""
        return {
            "hpoId": hpoId,
            "name": self._hpoTerms[hpoId].name,
            # "synonyms": list(set(self._hpoTerms[hpoId].synonyms.strip('"').split('","'))),
            # "defn": self._hpoTerms[hpoId].definition,
            "system": self.termToSystem(hpoId),
        }

    def get_parents(self, hpoId):
        return self._buHpoDag[hpoId]

    def get_children(self, hpoId):
        return self._tdHpoDag[hpoId]

    def get_all_hpo_ids(self):
        return set([hpoId for hpoId in self._hpoTerms])

    def verify_term(self, term):
        assert term in self._hpoTerms, "%s not in my hpoTerms" % term

    def phenotype_score(self, term_list1, term_list2):
        # for term in term_list1:
        #     self.verify_term(term)
        # for term in term_list2:
        #     self.verify_term(term)
        closure1 = self.term_ancestor_closure(term_list1)
        closure2 = self.term_ancestor_closure(term_list2)
        intersection = closure1.intersection(closure2)
        return self._phenotype_information(intersection)

    def _phenotype_information(self, term_list):
        return sum([self._termMarginalIc.get(t, 0) for t in term_list])

    def pruneTerms(self, termList):
        """Prune a list of terms to only include "leaf" terms that do not have
        a descendant on the list.  This provides a minimal representation such
        that closure(pruneTerms(termList))==closure(termList)."""
        prune = set() # set of terms to prune
        def helper(term):
            # Mark a term and its ancestors for pruning,
            # if it has not already been pruned.
            if term not in prune:
                prune.add(term)
                for p in self._buHpoDag.get(term,[]):
                    helper(p)

        closure = self.term_ancestor_closure(termList)
        for term in closure:
            for p in self._buHpoDag.get(term,[]):
                helper(p)

        return closure.difference(prune)

    def phenotypeComparison(self, refTerms, compareTerms):
        result = dict()
        compareClosure = self.term_ancestor_closure(compareTerms)
        compareMinimal = self.pruneTerms(compareClosure)

        def systemPhenotypeComparison(refTerms):
            """Helper that compares a single body system group of refTerms with compareTerms."""
            # Identify where the "compare terms" intersect with each refTerm t.
            # Group ref terms based on the intersection.
            sharedToRefTerms = collections.defaultdict(set)
            for t in refTerms:
                shared = frozenset(self.pruneTerms(compareClosure.intersection(self.term_ancestor_closure({t}))))
                sharedToRefTerms[shared].add(t)
            return sharedToRefTerms

        # Group the ref terms by body system.
        rtBySystem = collections.defaultdict(set)
        for rt in refTerms:
            rtBySystem[self.termToSystem(rt)].add(rt)

        # For each compare term that is shared, identify the leaf nodes that are support.
        mzSupport = dict()
        def support(term):
            if term not in mzSupport:
                supportTerms = self.term_descendant_closure({term}).intersection(compareClosure)
                # Identify the minimal terms that provide support;
                # list conditions (disease or animal gene disruption) for any terms, even non-leaf ones.
                mzSupport[term] = { "terms": [self.hpoTermDict(s) for s in self.pruneTerms(supportTerms)] }
            return mzSupport[term]

        # Within each system, group refTerms by where they intersect with the compare terms.
        for system, systemTerms in rtBySystem.items():
            systemSharedToRefTerms = systemPhenotypeComparison(systemTerms)
            result[system] = list()
            for compareShared, rts in systemSharedToRefTerms.items():
                shared = []
                for t in compareShared:
                    o = self.hpoTermDict(t)
                    o["support"] = support(t)
                    shared.append(o)
                terms = [self.hpoTermDict(t) for t in rts]
                result[system].append({"terms": terms, "shared": shared})

        return result

# Dictionary that maps from body system to the terms that group within that system.
# The systems are listed in priority order (highest to lowest).  If a term falls within
# multiple systems, it is placed in the highest priority system.
bodySystemTerms = collections.OrderedDict()
bodySystemTerms["Known Disease Pattern"] = ["HP:0000005"]
bodySystemTerms["Growth and development"] = ["HP:0001507", "HP:0001197", "HP:0000240"]
bodySystemTerms["Cancer"] = ["HP:0002664"]
bodySystemTerms["Eyes"] = ["HP:0000478"]
bodySystemTerms["Ears, nose, and throat"] = ["HP:0000598", "HP:0011389", "HP:0011452", "HP:0000366", "HP:0001600", "HP:0000600", "HP:0002087"]
bodySystemTerms["Cardiovascular"] = ["HP:0001626"]
bodySystemTerms["Gastrointestinal"] = ["HP:0011024", "HP:0002019", "HP:0002014", "HP:0011968"]
bodySystemTerms["Musculoskeletal"] = ["HP:0003011", "HP:0000924", "HP:0003549"]
bodySystemTerms["Neurologic"] = ["HP:0000707", "HP:0001250", "HP:0011446"]
bodySystemTerms["Endocrine"] = ["HP:0000818"]
bodySystemTerms["Respiratory"] = ["HP:0002086", "HP:0012252"]
bodySystemTerms["Genitourinary"] = ["HP:0000119", "HP:0000079", "HP:0000078"]
bodySystemTerms["Skin, hair, and nails"] = ["HP:0001574", "HP:0011138"]
bodySystemTerms["Hematologic/lymphatic"] = ["HP:0001871"]
bodySystemTerms["Immune"] = ["HP:0002715"]
bodySystemTerms["Face and skull"] = ["HP:0000271", "HP:0000929", "HP:0000234"]
bodySystemTerms["Metabolism"] = ["HP:0001939"]
bodySystemTerms["Other"] = ["HP:0000769"]

# List of body systems
BodySystems = list(bodySystemTerms.keys())


terms_file = os.path.join(__settings__.APP_HOME, 'onto/hpo/hpoTerms.txt')
dag_file = os.path.join(__settings__.APP_HOME, 'onto/hpo/hpoDag.txt')


def get_gene_to_hpo(dateX, dateY):
    rv = []
    with open(os.path.join(__settings__.APP_HOME, 'onto/genes/gene_to_hpo_X%s_Y%s.txt' % (dateX, dateY))) as f:
        for line in f:
            line = line.rstrip('\n').split('\t')
            rv.append((line[0], line[1]))
    return rv


dateXsave = None
dateYsave = None
phenotyper = None


def get_phenotyper(dateX, dateY):
    global dateXsave
    global dateYsave
    global phenotyper
    assert dateXsave is None or dateXsave == dateX
    dateXsave = dateX
    assert dateYsave is None or dateYsave == dateY
    dateYsave = dateY
    if phenotyper is not None:
        return phenotyper
    phenotyper = Phenotyper(terms_file, dag_file, get_gene_to_hpo(dateX, dateY))
    return phenotyper

def get_hpo_phenotyper():
    global phenotyper
    if phenotyper is not None:
        return phenotyper
    phenotyper = Phenotyper(terms_file, dag_file, os.path.join(__settings__.APP_HOME, 'onto/hpo/hpo_gene_to_hpo.tsv'))
    return phenotyper
