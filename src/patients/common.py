"""Common utility functions."""

import os
import sys
import collections
import numpy
import scipy.stats
import subprocess
from patients import linefile

"""Enumerated type for pysam CIGAR strings."""
class CIGAR:
    M = 0
    I = 1
    D = 2
    N = 3
    S = 4
    H = 5
    P = 6
    EQUAL = 7
    X = 8

SV_SUBFIELDS = ["IMPRECISE", "SVTYPE", "SVLEN", "END", "CIPOS", "CIEND"]

def abort(msg):
    """Display an error message and abort (exit with rc=1)."""
    sys.exit("%s" % (msg));

def expand_braces(s):
    """Expand a string into a list of strings using Bash-style brace expansion"

    Examples:
        expand_braces("Susie James")        --> ["Susie James"]
        expand_braces("S{usie} James")      --> ["Susie James"]
        expand_braces("S{usie,adie} James") --> ["Susie James", "Sadie James"]
        expand_braces("1{A,B,C}2{D,E}")     --> ["1A2D", "1A2E", "1B2D", "1B2E", "1C2D", "1C2E"]
    """

    first_open_brace = s.find("{")
    first_close_brace = s.find("}", first_open_brace)

    if first_open_brace == -1 or first_close_brace == -1:
        # Base case: no brace expression.  Return a single
        # expansion, the string literal.
        return [s]
    else:
        # Recursive case: expand the first brace expression
        # and append it to the expansion of the rest of the
        # string.

        prefix = s[:first_open_brace]  # the literal before the first brace expression
        expression = s[first_open_brace + 1:first_close_brace]  # the first brace expression
        the_rest = s[first_close_brace + 1:]  # the rest of the string
        the_rest_expansions = expand_braces(the_rest)

        expansions = []
        for val in expression.split(","):
            for the_rest_expansion in the_rest_expansions:
                expansions.append(prefix + val + the_rest_expansion)
        return expansions

def baseext(fn):
    """Get the base extension of a file.  The basic extension is the extension
    without the compression suffix."""
    base, ext = os.path.splitext(fn)
    return os.path.splitext(base)[1] if ext in (".gz", ".bz2", ".zip", ".Z", ".bgz") else ext


def set_default_signal_handlers():
    """Replace Python's handlers for SIGPIPE and SIGINT (ctrl+c) with POSIX default
    handlers. Python's handlers turn the signals into exceptions that pollute stderr
    with a stack trace. Most Unix programs terminate when receiving the signals."""
    import signal
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)
    signal.signal(signal.SIGINT, signal.SIG_DFL)


def uniquify(L):
    """Remove duplicates from a list while preserving element
    order, sorted by first appearance."""
    seen = set()
    for x in L:
        if x not in seen:
            seen.add(x)
            yield x

class CaseInsensitiveDict(dict):
    """Dictionary where the keys are case-insensitive."""
    def __getitem__(self, key):
        return super(CaseInsensitiveDict, self).__getitem__(key.lower())

    def __setitem__(self, key, value):
        return super(CaseInsensitiveDict, self).__setitem__(key.lower(), value)

def range_overlap(a, b, onebased=False):
    ''' Checks if a range is overlapping (e.g. (1,2), (2,3) --> false, (1,3), (2,4) --> true'''
    a, b = sorted([a, b])
    if onebased:
        a = (a[0] - 1, a[1])
        b = (b[0] - 1, b[1])
    if(a[0] >= a[1]):
        raise ValueError("Range (%s) needs to have start (%s) < end (%s)" % ((a, a[0], a[1])))
    if(b[0] >= b[1]):
        raise ValueError("Range (%s) needs to have start (%s) < end (%s)" % ((b, b[0], b[1])))
    return (b[0] < a[1])

def range_overlap_fraction(range1, range2, onebased=False):
    '''
        if onebased=True
        range_overlap_fraction((4, 6), (1, 2)) == (0/3, 0/2)
        range_overlap_fraction((4, 6), (2, 3)) == (0/3, 0/2)
        range_overlap_fraction((4, 6), (3, 4)) == (1/3, 1/2)
        range_overlap_fraction((4, 6), (4, 5)) == (2/3, 2/2)
        range_overlap_fraction((4, 6), (5, 6)) == (2/3, 2/2)
        range_overlap_fraction((4, 6), (6, 7)) == (1/3, 1/2)
        range_overlap_fraction((4, 6), (7, 8)) == (0/3, 0/2)
        range_overlap_fraction((4, 6), (8, 9)) == (0/3, 0/2)
    '''
    start1, end1 = range1
    start2, end2 = range2
    if not onebased:
        start1 += 1
        start2 += 1
    r1_span = end1 - start1 + 1
    r2_span = end2 - start2 + 1
    overlap = max(0, min(end1, end2) - max(start1, start2) + 1)
    return (overlap / r1_span, overlap / r2_span)  # fraction range1 overlapping range2, fraction of range2 overlapping range1

def max_span(a, b):
    ''' Returns the maximum span of two ranges (e.g. (1,2), (5,6) --> (1, 6)) '''
    return (min(a[0], b[0]), max(a[1], b[1]))

def merge_ranges(ranges):
    ''' Return a set of ranges with overlaps merged into one large range.
    Uses the range_overlap to check if an item is overlapping. '''
    ranges = sorted(ranges)
    first = ranges[0] if len(ranges) else []
    for r in ranges[1:]:
        if(range_overlap(first, r)):
            first = max_span(first, r)
        else:
            yield first
            first = r
    if(first):
        yield first

def load_map(fn_or_stream, sep='\t', c1=0, c2=1, c1_sep=None, c2_sep=None, c1_singleton=False, c2_singleton=False):
    ''' Reads columns (zero indexed) c1 and c2 of a file and makes a mapping between them.
    If c1_sep and/or c2_sep are defined then it will assuming that column has multiple items demarcated by the sep.
    If c1_singleton and/or c2_singleton are set to True, then instead of a mapping to a set, a flattened singleton is returned.
    (i.e. It will require the mappings to be one to one.)
    E.g c1_singleton means that col2_to_col1 will return a mapping of col2 to col1 singletons instead of sets, vice versa. '''
    col1_to_col2 = collections.defaultdict(set)
    col2_to_col1 = collections.defaultdict(set)
    for line in linefile.open(fn_or_stream):
        data = line.strip().split(sep)
        col1_data, col2_data = data[c1], data[c2]
        col1_data = col1_data.strip().split(c1_sep) if c1_sep is not None else [col1_data]
        col2_data = col2_data.strip().split(c2_sep) if c2_sep is not None else [col2_data]
        for col1 in col1_data:
            for col2 in col2_data:
                col1_to_col2[col1].add(col2)
                col2_to_col1[col2].add(col1)
    if c2_singleton:
        for key, val in col1_to_col2.items():
            if len(val) != 1:
                raise ValueError("C1 value [%s] maps to multiple C2 values [%s]" % (key, c2_sep.join(val) if c2_sep else ','.join(val)))
            col1_to_col2[key] = val.pop()

    if c1_singleton:
        for key, val in col2_to_col1.items():
            if len(val) != 1:
                raise ValueError("C2 value [%s] maps to multiple C1 values [%s]" % (key, c1_sep.join(val) if c1_sep else ','.join(val)))
            col2_to_col1[key] = val.pop()

    return col1_to_col2, col2_to_col1

def load_ordered_dict(fn_or_stream):
    output = collections.OrderedDict()
    for line in linefile.open(fn_or_stream):
        key, value = line.strip().split("\t")
        output[key] = value.split(",")
    return output

def get_codon_coords(isoform, location, isoform_details):
    """
    inputs:
    - isoform - the isoform that the variant is on
    - location - the genomics location of the variant we want amino acid location for
    - isoform_details - the dictionary with key - isoform_id and value - exon_starts, exon_ends
    outputs:
    - position - the position 0, 1, 2 that the base pair is in the amino acid
    - coords - the left center and right coords for the codon
    - distance_to_leftmost_cds - the distance to the leftmost cds of the isoform
    - total_exon_length - the total length of the isoform
    - distance_to_exonstart - distance of the SNP location to the exon start
    - distance_to_exonend - distance of the SNP location to the exon end
    """
    EXON_STARTS = isoform_details.get(isoform, {}).get("EXON_STARTS", [])
    EXON_ENDS = isoform_details.get(isoform, {}).get("EXON_ENDS", [])
    LEFTMOST_CDS = isoform_details.get(isoform, {}).get("LEFTMOST_CDS")
    RIGHTMOST_CDS = isoform_details.get(isoform, {}).get("RIGHTMOST_CDS")

    if len(EXON_STARTS) != len(EXON_ENDS):
        raise ValueError("the length of EXON_STARTS and EXON_ENDS do not equal each other.")

    exon_start, exon_end, exon_num = None, None, None
    # loop through each exon and figure out which exon the SNP is located in
    for i, (start, end) in enumerate(zip(EXON_STARTS, EXON_ENDS)):
        if LEFTMOST_CDS >= start and LEFTMOST_CDS <= end:
            exon_start = i
        if RIGHTMOST_CDS >= start and RIGHTMOST_CDS <= end:
            exon_end = i
        if location > start and location <= end:
            exon_num = i

    if location <= LEFTMOST_CDS or location > RIGHTMOST_CDS:
        raise ValueError("the chromosome location is not not in boundary of cds.")

    if exon_num is None:
        # check if exon is None and error
        raise ValueError("the chromosome location specified does not lie inside any exon in this isoform: %s" % isoform)

    # loop through and compute teh distance from the leftmost cds and the distance from the start of the exon
    distance_to_leftmost_cds = 0
    distance_to_exonstart = 0
    total_exon_length = 0
    distance_to_exonend = 0

    for i, (start, end) in enumerate(zip(EXON_STARTS, EXON_ENDS)):
        start = LEFTMOST_CDS if i == exon_start else start
        end = RIGHTMOST_CDS if i == exon_end else end

        # check if the current exon is an exon that is included in the isoform
        if i >= exon_start and i <= exon_end:
            # current exon is the exon with the loci
            if i == exon_num:
                distance_to_leftmost_cds = total_exon_length + location - start - 1  # needs to be 0 indexed because we are doing mod
                distance_to_exonstart = location - start - 1  # need to be 0 indexed based on the check we do below
                distance_to_exonend = end - location  # needs to be 1 indexed since we want the last element in the tuple to mod to 0
            total_exon_length += end - start  # keep track of the length of all exons so far

    # check the length of the base pairs included in the exon is a multiple of 3. otherwise raise an error
    if total_exon_length % 3 != 0:
        raise ValueError("the size of the exon is not a multiple of 3. This means something is wrong with our coordinates.")

    # calculate the coordinates of the SNP based on the exon and leftmost cds information
    position = distance_to_leftmost_cds % 3
    if position == 0:
        # compute the coordinates based on how close we are to the end of the exon
        if  distance_to_exonend == 0:
            coords = (location, EXON_STARTS[exon_num + 1] + 1, EXON_STARTS[exon_num + 1] + 2)
        elif distance_to_exonend == 1:
            coords = (location, location + 1, EXON_STARTS[exon_num + 1] + 1)
        else:
            coords = (location, location + 1, location + 2)
    elif position == 1:
        # check that the SNP is not close to the exon start and not close to the end of the exon
        if distance_to_exonstart == 0:
            coords = (EXON_ENDS[exon_num - 1], location, location + 1)
        elif distance_to_exonend == 0:
            coords = (location - 1, location, EXON_STARTS[exon_num + 1] + 1)
        else:
            coords = (location - 1, location, location + 1)
    elif position == 2:
        # compute the coordinates based on how close we are to the start of the exon
        if distance_to_exonstart == 0:
            coords = (EXON_ENDS[exon_num - 1] - 1, EXON_ENDS[exon_num - 1], location)
        elif distance_to_exonstart == 1:
            coords = (EXON_ENDS[exon_num - 1], location - 1, location)
        else:
            coords = (location - 2, location - 1, location)

    return position, coords, distance_to_leftmost_cds, total_exon_length, distance_to_exonstart, distance_to_exonend

def call_maf_extract(region_fmt_string, maf_file):
    """
    call_maf_extract - function call to the mafExtract utility
    inputs:
     - region_fmt_string - a string giving the chrom start and end for the 3 base pairs we want MAF data
     - maf_file - maf_file name to use as input to the mafExtract command
    outputs:
     - output - the raw data from mafExtract
    """

    cmd = ["mafExtract", maf_file, "stdout", "-regionList=stdin"]
    proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    proc.stdin.write(bytes(region_fmt_string, encoding='utf-8'))
    output = proc.communicate()[0]
    output = output.decode('utf-8')
    return output

def get_codon_dict_from_mafextract(output):
    """
    get_codon_dict_from_mafextract - parses the output of the mafExtract and gets the codon associated with each species
    inputs:
     - output - the output from the mafExtract call
    outputs:
     - codon_dict - dictionary with key - species name and value - the codon (3 base pairs)
    """
    lines = output.split("\n")

    # parse output of mafExtract
    # loop through all species and keep track of the base pairs in the codon for each species
    codon_dict = {}
    count = 0
    for line in lines:
        if line.startswith("s "):
            maf_line = line.strip().split()
            align_char = maf_line[6]
            align_char = align_char.strip("-")
            align_char = align_char.upper()
            if align_char in ['A', 'T', 'C', 'G']:
                species = maf_line[1].split(".")[0]
                codon_dict[species] = codon_dict.get(species, "") + align_char
        elif line == "a score=":
            # start of the new maf block
            count += 1

    # remove all keys that have a value of less than or greater than 3 base pairs
    species_list = list(codon_dict.keys())
    for species in species_list:
        if len(codon_dict.get(species, "")) != 3:
            codon_dict.pop(species)

    return codon_dict

def load_named_spans(fn_or_stream):
    name_to_spans = collections.defaultdict(set)
    for line in linefile.open(fn_or_stream):
        contig, contig_start, contig_end, name = line.strip().split()
        name_to_spans[name].add((contig, int(contig_start), int(contig_end)))
    return name_to_spans

def load_aminoacid_matrix(filename):
    f = linefile.open(filename)
    aminoacid_score_map = {}
    to_aas = next(f).strip().split('\t')
    for l in f:
        cols = l.rstrip('\n').split("\t")
        from_aa = cols[0]
        for to_aa, score in zip(to_aas, cols[1:]):
            aminoacid_score_map[(from_aa, to_aa)] = float(score)
    return aminoacid_score_map

def load_isoform_details(isoform_details_filename):
    """
    inputs:
     - isoform_details_filename - file where each row represents the details about each isoform
    outputs:
     - isoform_details_dict - dictionary with key - isoform_id and value - LEFTMOST_CDS, EXON start sites
    """
    isoform_details_dict = {}
    isoform_file = linefile.open(isoform_details_filename)
    for line in isoform_file:
        isoform_details = line.rstrip().split("\t")
        isoform_details_dict[isoform_details[1]] = {
                                                    "EXON_STARTS":[int(start) for start in isoform_details[9].rstrip(",").split(",")],
                                                    "EXON_ENDS": [int(end) for end in isoform_details[10].rstrip(",").split(",")],
                                                    "LEFTMOST_CDS": int(isoform_details[6]),
                                                    "RIGHTMOST_CDS": int(isoform_details[7]),
                                                    "REVERSE": True if isoform_details[3] == "-" else False,
                                                    }
    return isoform_details_dict

def reverse_complement(base_pairs):
    complement = {"A":"T", "C":"G", "G":"C", "T":"A"}
    rc = ""
    for base_pair in base_pairs[::-1]:
        rc += complement[base_pair]
    return rc

def parse_hgvs_aa(amino_acid_seq):
    # parse an HGVS amino acid variant description into a (<from_aa>,<to_aa>) tuple
    from_aa = amino_acid_seq[2]
    to_aa = amino_acid_seq[-1]

    try:
        codon_position = int(amino_acid_seq[3:-1])
    except:
        raise ValueError("Unable to parse the codon position in aminoacid annotation properly %s" % amino_acid_seq)

    # if the from_aa and to_aa are incorrectly parse -> raise an error
    if not from_aa.isalpha() or not to_aa.isalpha():
        raise ValueError("Unable to parse the hgvs aminoacid annotation properly %s" % amino_acid_seq)
    return from_aa, to_aa, codon_position

def parse_hgvs_nuc(nuc_seq):
    # parse an HGVS nuc variant description into a (<from_nuc>,<to_nuc>) tuple
    pos_nuc = nuc_seq.strip().split(".")
    nuc = pos_nuc[1].strip("+-*").strip("1234567890")
    from_nuc, to_nuc = nuc.split(">")

    # if the from_nuc and to_nuc are incorrectly parsed -> raise an error
    if not from_nuc.isalpha() or not to_nuc.isalpha():
        raise ValueError("Unable to parse the hgvs nucleotide annotation properly %s" % nuc_seq)
    return from_nuc, to_nuc

def compute_residual(x, Y):
    """Given a pair of values x, Y performs linear regression and returns Y_hat, e_hat (residual), var_hat, h (leverage) and sr (studentized residual)."""
    n = len(x)
    m = 2
    Y = numpy.array(Y, dtype=float)
    x = numpy.array(x, dtype=float)
    X = numpy.matrix([numpy.ones(n), x]).T
    b, a = numpy.linalg.lstsq(X, Y)[0]
    Y_hat = a * x + b
    h = 1 / n + ((x - numpy.average(x)) ** 2) / sum((x - numpy.average(x)) ** 2)  # leverage
    e_hat = Y - Y_hat
    var_hat = e_hat * e_hat  # array has squared residuals
    var_hat = sum(var_hat) - var_hat  # array has sum of squared residuals without the ith value (since it is subtracted out)
    var_hat = var_hat / (n - m - 1)
    sr = e_hat / (numpy.sqrt(var_hat * (1 - h)))  # student residuals
    return Y_hat, e_hat, var_hat, h, sr

def compute_percentiles(x, kind='rank'):
    ''' kind =  {‘rank’, ‘weak’, ‘strict’, ‘mean’} - see http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.percentileofscore.html for definition '''
    if kind == 'rank':
        return ((scipy.stats.rankdata(x, "average")) / len(x)) * 100
    elif kind == 'weak':
        return (scipy.stats.rankdata(x, "max")) / len(x) * 100
    elif kind == 'strict':
        return (scipy.stats.rankdata(x, "min") - 1.0) / len(x) * 100
    elif kind == 'mean':
        return ((scipy.stats.rankdata(x, "max")) / len(x) * 100 + (scipy.stats.rankdata(x, "min") - 1.0) / len(x) * 100) / 2.0
    else:
        raise ValueError("Percentile kind parameter needs to one of {‘rank’, ‘weak’, ‘strict’, ‘mean’} not (%s)." % (kind))

class Isoform(object):
    ''' A container class to hold on to information about the Isoform '''
    def __init__(self, isoform_id, gene_name, chrom, strand, txStart, txEnd, cdsStart, cdsEnd, exonStarts, exonEnds):
        self.isoform_id = isoform_id
        self.gene_name = gene_name
        self._chrom = chrom
        self.strand = '-' if strand == '-' else '+'
        self.txStart = int(txStart)
        self.txEnd = int(txEnd)
        self.cdsStart = int(cdsStart)
        self.cdsEnd = int(cdsEnd)
        self.exonStarts = numpy.array(list(map(int, exonStarts.rstrip(',').split(','))))
        self.exonEnds = numpy.array(list(map(int, exonEnds.rstrip(',').split(','))))
        self.coding_bases = {}
        for start, end in zip(self.exonStarts, self.exonEnds):
            self.coding_bases.update({}.fromkeys(range(start, end)))

        for k in list(self.coding_bases.keys()):
            if((k < self.cdsStart) or (k >= self.cdsEnd)):
                del self.coding_bases[k]
        self.coding_bases_array = numpy.array(list(self.coding_bases.keys()))
        self.num_coding_bases = len(self.coding_bases_array)
        self.has_multiple_locations = False

    def __repr__(self):
        return "IsoformDetails(isoform_id=%s, chrom=%s, strand=%s, txStart=%d, txEnd=%d, cdsStart=%d, cdsEnd=%d)" \
               % (self.isoform_id, self.chrom, self.strand, self.txStart, self.txEnd, self.cdsStart, self.cdsEnd)

    @classmethod
    def parse(cls, line):
        '''parse the UCSC refGene.txt format into an isoform'''
        (bin_num, name, chrom, strand, txStart, txEnd, cdsStart, cdsEnd, \
         exonCount, exonStarts, exonEnds, score, name2, cdsStartStat, cdsEndStat, exonFrames) = line.strip().split()
        return cls(name, name2, chrom, strand, txStart, txEnd, cdsStart, cdsEnd, exonStarts, exonEnds)

    @property
    def chrom(self):
        return "%s.multi" % (self._chrom) if(self.has_multiple_locations) else self._chrom

    @property
    def coding(self):
        return self.cdsStart != self.cdsEnd

    @chrom.setter
    def chrom(self, value):
        self._chrom = value

    @property
    def exonCount(self):
        assert(len(self.exonStarts) == len(self.exonEnds))
        return len(self.exonStarts)

    def is_coding_base(self, pos):
        return (pos in self.coding_bases)

    def coding_bases_upstream(self, pos):
        '''number of coding bases that occur before "pos"'''
        if(self.strand == '-'):
            return (self.coding_bases_array >= pos + 1).sum()
        return (self.coding_bases_array < pos).sum()

    def coding_bases_downstream(self, pos):
        '''number of coding bases that occur after "pos" (including)'''
        return self.num_coding_bases - self.coding_bases_upstream(pos)

    def fraction_coding_upstream(self, pos):
        '''excluding "pos", what fraction of the coding gene is upstream'''
        return self.coding_bases_upstream(pos) / float(self.num_coding_bases)

    def fraction_coding_downstream(self, pos):
        '''including "pos" and all following bases, what fraction of the coding gene is downstream'''
        return self.coding_bases_downstream(pos) / float(self.num_coding_bases)

    def tx_coordinate(self, allele):
        if(allele):
            if(self.strand == '-'):
                return self.txEnd - 1 - allele
            else:
                return allele - self.txStart
        return None

    def genome_coordinate(self, allele):
        if(allele):
            if(self.strand == '-'):
                return self.txEnd - 1 - allele
            else:
                return allele + self.txStart
        return None

class Bed:
    """A basic BED interval"""
    def __init__(self, chrom, chromStart, chromEnd, name=None):
        self.chrom = chrom
        self.chromStart = chromStart
        self.chromEnd = chromEnd
        self.name = name

    def clip(self, chromSizes):
        """Clip chromStart and chromEnd to fit within the chromosome"""
        self.chromStart = max(self.chromStart, 0)
        self.chromEnd = min(self.chromEnd, chromSizes[self.chrom])

    def length(self):
        return self.chromEnd - self.chromStart

    def __str__(self):
        cols = [self.chrom, self.chromStart, self.chromEnd]
        if self.name is not None:
            cols.append(self.name)
        return "\t".join([str(x) for x in cols])

    def __repr__(self):
        cols = [self.chrom, self.chromStart, self.chromEnd]
        if self.name is not None:
            cols.append(self.name)
        return "Bed(%s)" % (",".join([repr(x) for x in cols]))

    @staticmethod
    def parse(line, sep=None):
        """Create a Bed object from a string definition.
            - line: string to parse
            - sep: field separator (default: any whitespace)
        """
        cols = line.split(sep)
        chrom = cols[0]
        chromStart = int(cols[1])
        chromEnd = int(cols[2])
        return Bed(chrom, chromStart, chromEnd)

