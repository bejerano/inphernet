#! /usr/bin/env python3

from __future__ import print_function
import sys
import os
from collections import defaultdict, namedtuple
import logging
import tabix

import __settings__

hom = "hom"
het = "het"
null = "null"
unk = "?"

handler = logging.StreamHandler(sys.stderr)
logger = logging.getLogger(__file__)
logger.addHandler(handler)
level = logging.getLevelName('DEBUG')
logger.setLevel(level)


class Variant:
    __slots__ = ['chrom', 'start', 'end', 'ref', 'alt', 'exac_hmct', 'exac_alct', 'kgp_alfq',
                 'kgp_alct', 'kgp_hmct', 'exac_alfq', 'gene_position', 'gene_symbol', 'aa_change',
                 'exonic_function', 'zygosity', 'ensembl_id', 'mcap', 'mcap_100bp']

    def __str__(self):
        return "variant %s" % ','.join(str(getattr(self, x)) for x in self.__slots__)

    def get_semantic_effect(self):
        gene_positions = self.gene_position.split(';')
        exonic_funcs = self.exonic_function.split(';')
        # just take first matching variant type; in the end, it doesn't even matter ;)
        if 'exonic' in gene_positions:
            if any(x.startswith('frameshift') for x in exonic_funcs):
                return 'frameshift'
            if any(x.startswith('nonframeshift') for x in exonic_funcs):
                return 'nonframeshift'
            if any(x.startswith('nonsynonymous') for x in exonic_funcs):
                return 'nonsynonymous'
            if any(x.startswith('stopgain') for x in exonic_funcs):
                return 'stopgain'
            if any(x.startswith('stoploss') for x in exonic_funcs):
                return 'stoploss'
            if any(x.startswith('unknown') for x in exonic_funcs):
                # who knows what this BS is ... let's just say nonframeshift
                return 'nonframeshift'
        if 'splicing' in gene_positions:
            return 'splicing'
       
        #assert False, (str(self), gene_positions, exonic_funcs)
        return None

    def __init__(self, chrom, start, end, ref, alt, exac_hmct, exac_alct, exac_alfq,
                 gene_position, gene_symbol, aa_change, exonic_function,
                 zygosity, ensembl_id, mcap, mcap_100bp, kgp_alfq, kgp_alct, kgp_hmct):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.ref = ref
        self.alt = alt
        self.exac_hmct = exac_hmct
        self.exac_alct = exac_alct
        self.exac_alfq = exac_alfq
        self.gene_position = gene_position
        self.gene_symbol = gene_symbol
        self.exonic_function = exonic_function
        self.aa_change = aa_change
        self.zygosity = zygosity
        self.ensembl_id = ensembl_id
        self.mcap = mcap
        self.mcap_100bp = mcap_100bp
        self.kgp_alfq = kgp_alfq
        self.kgp_alct = kgp_alct
        self.kgp_hmct = kgp_hmct


mcap_tabix = None


def get_mcap(chrom, start, end, ref, alt):
    if ref == "-" or alt == "-":
        return None
    if len(ref) > 1:
        return None
    if len(alt) > 1:
        return None
    global mcap_tabix
    if not mcap_tabix:
        mcap_tabix = tabix.open(os.path.join(__settings__.APP_HOME, 'data/onto/mcap/mcap.annovar.bgz'))
    records = [x for x in mcap_tabix.querys("%s:%d-%d" % (chrom, start, start+1))]
    assert len(records) <= 3, records
    if not records:
        return None
    records = [x for x in records if x[4] == alt]
    if len(records) == 0:
        return None
    record = records[0]
    if record[5] == '.':
        return None
    return float(record[5])


def get_mcap_100bp(chrom, start, end):
    global mcap_tabix
    if not mcap_tabix:
        mcap_tabix = tabix.open(os.path.join(__settings__.APP_HOME, 'data', 'onto', 'mcap', 'mcap.annovar.bgz'))
    records = [x for x in mcap_tabix.querys("%s:%d-%d" % (chrom, max(0, start-50), end+50))]
    mcaps = []
    for record in records:
        if record[5] == '.':
            continue
        mcaps.append(float(record[5]))
    if not mcaps:
        return None
    return max(mcaps)
    #return sum(mcaps) / len(mcaps)


def filter_variants_hmct(variants, hmct_cutoff):
    rv = []
    for v in variants:
        if v.exac_hmct is not None and v.exac_hmct > hmct_cutoff:
            logger.info("[exac_hmct] variant %s: hmct > %d, dropping" % (str(v), hmct_cutoff))
            continue
        rv.append(v)
    return rv


def filter_variants_alfq(variants, alfq_cutoff):
    rv = []
    
    for v in variants:
        if v.exac_alfq is not None and v.exac_alfq > alfq_cutoff:
            logger.info(v.exac_alfq)
            logger.info("[exac_alfq] variant %s: alfq > %f, dropping" % (str(v), alfq_cutoff))
            continue
        if v.kgp_alfq is not None and v.kgp_alfq > alfq_cutoff:
            logger.info(v.kgp_alfq)
            logger.info("[kgp_alfq] variant %s: alfq > %f, dropping" % (str(v), alfq_cutoff))
            continue
        rv.append(v)
    return rv


RvGene = namedtuple('RvGene', ['eid', 'inheritance_mode', 'variants'])


def genes_to_rv_genes(gene_to_variants,
                      gene_to_max_num_broken_copies):
    rv = {}
    for eid in gene_to_variants:
        rv_gene = RvGene(eid=eid,
                         inheritance_mode=gene_to_max_num_broken_copies[eid],
                         variants=set(gene_to_variants[eid]))
        rv[eid] = rv_gene
    return rv


def filter_dominant_variants_by_freq(gene_to_variants,
                                     filter_by_count,
                                     alct_cutoff,
                                     dominant_alfq_cutoff):
    # print("filtering by count: %s" % str(filter_by_count))
    rv_gene_to_variants = {}
    gene_to_max_num_broken_copies = {}
    for eid in gene_to_variants:
        max_num_broken_copies = 0
        for v in gene_to_variants[eid]:
            if v.zygosity == "hom":
                max_num_broken_copies += 2
            else:
                max_num_broken_copies += 1
        gene_to_max_num_broken_copies[eid] = max_num_broken_copies

        if max_num_broken_copies <= 1:
            variants_list = []
            for v in gene_to_variants[eid]:
                if filter_by_count:
                    # logger.info("Filtering by count; exac_alct=%d and alct_cutoff=%d" % (v.exac_alct, alct_cutoff))
                    if v.exac_alct is not None and v.exac_alct > alct_cutoff:
                        logger.info("[exac_dominant_alct] variant %s: "
                                    "alct > %d, dropping" % (str(v), alct_cutoff))
                        continue
                    if v.kgp_alct is not None and v.kgp_alct > alct_cutoff:
                        logger.info("[kgp_dominant_alct] variant %s: "
                                    "alct > %d, dropping" % (str(v), alct_cutoff))
                        continue
                        
                    if v.exac_alfq is not None and v.exac_alfq > dominant_alfq_cutoff:
                        logger.info("[exac_dominant_alfq] variant %s: "
                                    "alfq > %d, dropping" % (str(v), dominant_alfq_cutoff))
                        continue
                    if v.kgp_alfq is not None and v.kgp_alfq > dominant_alfq_cutoff:
                        logger.info("[kgp_dominant_alfq] variant %s: "
                                "alfq > %d, dropping" % (str(v), dominant_alfq_cutoff))
                        continue
                variants_list.append(v)
            if variants_list:
                rv_gene_to_variants[eid] = variants_list
        else:
            rv_gene_to_variants[eid] = gene_to_variants[eid]
    return gene_to_max_num_broken_copies, rv_gene_to_variants


def group_by_gene(variants):
    rv = defaultdict(lambda: set())
    for v in variants:
        rv[v.ensembl_id].add(v)
    return rv


def get_annotated_variants(basedir, individual):
    with open(os.path.join(basedir, '%s.variants.txt' % individual)) as f:
        variants = []
        for i, line in enumerate(f):
            if i == 0:
                parse_first_line(line)
            else:
                parse_annotated_variant(line, variants)
    return variants


def parse_annotated_variant(line, variants):
    chrom, start, end, ref, alt, exac_hmct, exac_alct, kgp_alfq, kgp_alct, kgp_hmct, \
        exac_alfq, gene_position, gene, exonic_function, aa_change, \
        ensembl_ids, zygosity = line.rstrip('\n').split('\t')
    start = int(start)
    end = int(end)
    exac_hmct = int(exac_hmct)
    exac_alct = int(exac_alct)
    exac_alfq = float(exac_alfq)
    kgp_hmct = int(kgp_hmct)
    kgp_alct = int(kgp_alct)
    kgp_alfq = float(kgp_alfq)
    # this is NOT a bed file ... annovar does start == end == vcf_pos
    mcap = get_mcap(chrom, start, end, ref, alt)
    if mcap is None:
        mcap = get_mcap_100bp(chrom, start, end)
    mcap_100bp = get_mcap_100bp(chrom, start, end)
    ensembl_ids = ensembl_ids.split(';')
    for ensembl_id in ensembl_ids:
        v = Variant(chrom=chrom, start=start, end=end, ref=ref, alt=alt,
                    exac_hmct=exac_hmct, exac_alct=exac_alct, exac_alfq=exac_alfq,
                    gene_position=gene_position,
                    gene_symbol=gene, exonic_function=exonic_function, aa_change=aa_change,
                    zygosity=zygosity, ensembl_id=ensembl_id,
                    mcap=mcap, mcap_100bp=mcap_100bp,
                    kgp_alfq=kgp_alfq, kgp_alct=kgp_alct, kgp_hmct=kgp_hmct)
        if exonic_function.lower() == "unknown":
            logger.info('[unknown_function] skipping variant %s' % str(v))
            continue
        variants.append(v)

def parse_first_line(line):
    expected_line = "#Chrom,Start,End,Ref,Alt,ExAC_HMCT,ExAC_ALCT,KGP_ALFQ," \
                    "KGP_ALCT,KGP_HMCT,ExAC_ALFQ,GenePosition," \
                    "Gene,ExonicFunc,AAChange,EnsemblID,Zygosity".split(',')
    line = line.rstrip('\n').split('\t')
    if line != expected_line:
        logger.info(line)
        assert False, line


def get_patient_eids(basedir, individual, alfq_cutoff, dominant_alfq_cutoff,
                     filter_by_count, hmct_cutoff=None, alct_cutoff=None):
    variants = get_annotated_variants(basedir, individual)

    if filter_by_count:
     #   logger.info("Filtering by homozygous count %d" % hmct_cutoff)
        variants = filter_variants_hmct(variants, hmct_cutoff)
      #  logger.info("After homozygous filtering: %d variants" % len(variants))
    logger.info("Number of variants in input (pre): %d" % len(variants))
    variants = filter_variants_alfq(variants, alfq_cutoff=alfq_cutoff)
    logger.info("Number of variants in input: %d" % len(variants))
    gene_to_variants = group_by_gene(variants)
    gene_to_max_num_broken_copies, gene_to_variants = \
        filter_dominant_variants_by_freq(gene_to_variants,
                                         filter_by_count=filter_by_count,
                             #            filter_by_count=False,
                                         alct_cutoff=alct_cutoff,
                                         dominant_alfq_cutoff=dominant_alfq_cutoff)
    rv_genes = genes_to_rv_genes(gene_to_variants, gene_to_max_num_broken_copies)
    return rv_genes


def get_patient_hpo_ids( patient):
    return open(patient, 'r').readlines()[0].rstrip().split('\t')[-1].replace('{','').replace('}', '').split(',')

