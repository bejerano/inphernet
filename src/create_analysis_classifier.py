#! /cluster/u/byoo1/software/miniconda/bin/python3
from __future__ import print_function

from collections import namedtuple
import sys
import os
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
import pickle
import logging
from random import shuffle
import errno
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import mutual_info_classif
import __settings__
from phrank import Phrank
from util import tail
from analysis import parse_solution

handler = logging.StreamHandler(sys.stderr)
logger = logging.getLogger(__file__)
logger.addHandler(handler)
level = logging.getLevelName('DEBUG')
logger.setLevel(level)


def num_true_positives(truth, predicted, max_tp):
    assert len(truth) == len(predicted)
    return min(sum(t == 1 and p == 1 for t, p in zip(truth, predicted)), max_tp)


def num_false_positives(truth, predicted):
    assert len(truth) == len(predicted)
    return sum(t == 0 and p == 1 for t, p in zip(truth, predicted))


def split_cv(iterable, num):
    lst = [x for x in iterable]  # [:100]
    splits = [lst[i::num] for i in range(num)]
    for i in range(len(splits)):
        yield [x for y in splits[0:i] + splits[i + 1:] for x in y], splits[i]


def split_list(split, iterable):
    lst = [x for x in iterable]
    shuffle(lst)
    i = int(len(lst) * split)
    split0 = lst[:i]
    split1 = lst[i:]
    return split0, split1


def flatten(l):
    return [x for y in l for x in y]


def load_features(patient_name, feature_names, features_path):
    test_feature_matrix = []
    for feature_name in feature_names:
        features_filename = os.path.join(features_path, patient_name + "." + feature_name + ".pkl")
        with open(features_filename, 'rb') as f:
            matrix = []
            for x in pickle.load(f):
                if x == 'None':
                    matrix.append(-1)
                else:
                    matrix.append(x)
            test_feature_matrix.append(matrix)
    return list(map(list, zip(*test_feature_matrix)))  # this is a weird transpose syntax


def load_labels(patient_name, features_path):
    with open(os.path.join(features_path, patient_name + ".labels.pkl"), 'rb') as f:
        labels = pickle.load(f)
        return labels


def load_index_to_entry(patient_name, features_path):
    with open(os.path.join(features_path, patient_name + ".index_to_entry.pkl"), 'rb') as f:
        indices = pickle.load(f)
        return indices


def load_match_scores(patient_name, match_score_feature_name, features_path):
    match_scores_filename = os.path.join(features_path, patient_name + "." + match_score_feature_name + ".pkl")
    with open(match_scores_filename, 'rb') as f:
        return pickle.load(f)

def remove_unused( features, indicies):
    return_list = []
    for i in indicies:
        d = {}
        for j in features:
            if j in i._fields:
                d[j] = getattr(i,j)
            elif j + "_score" in i._fields:
                d[j+ "_score"] = getattr(i,j + "_score")
        d["num_affected"] = i.num_affected
        d["ensembl_id"] = i.ensembl_id
        d["gene_name"] = i.gene_name
        return_list.append(namedtuple('TraceEntry', d.keys())(**d) )
    return return_list 

class Patient:

    name = None
    solution = None

    train_features = None
    train_labels = None
    train_index_to_entry = None

    test_features = None
    test_labels = None
    test_index_to_entry = None

    sorted_entries = None
    match_scores = None

    def __init__(self, solution):
        self.solution = solution
        self.name = solution.patient

    def load_train_features(self, feature_names, train_features_path):
        logger.info("Loading patient %s training features" % self.name)
        self.train_features = load_features(self.name, feature_names, train_features_path)

    def load_test_features(self, feature_names, test_features_path):
        logger.info("Loading patient %s test features" % self.name)
        self.test_features = load_features(self.name, feature_names, test_features_path)
        #self.match_scores = load_match_scores(self.name, "match_score_over_patient_max", test_features_path)

    def load_train_labels(self, train_features_path):
        logger.info("Loading patient %s training labels" % self.name)
        self.train_labels = load_labels(self.name, train_features_path)

    def load_test_labels(self, test_features_path):
        logger.info("Loading patient %s test labels" % self.name)
        self.test_labels = load_labels(self.name, test_features_path)

    def load_test_index_to_entry(self, test_features_path, features):
        logger.info("Loading patient %s test index to entry" % self.name)
        self.test_index_to_entry  = load_index_to_entry(self.name, test_features_path)        
        #self.test_index_to_entry = remove_unused(features, temp)
        #self.test_index_to_entry = temp

    def run(self, clf):
        probas = clf.predict_proba(self.test_features)[:, 1]
        scores = []
        for i in range(len(self.test_features)):
            scores.append(probas[i])
        self.sorted_entries = sorted([(scores[i], self.test_index_to_entry[i])
                                      for i in range(len(self.test_features))])[::-1]

    def compute_similarity_score( self, p_name, outdir, phrank, gene, feature_names):
        n_feature_mappings = {
                     'Paralog': 'n_paralog_by_disease',
                     'Ortholog': 'n_ortholog_by_gene_only_hp', 
                     'Interaction_disease': 'n_interaction_filtered_by_disease',
                     'Interaction_gene': 'n_interaction_filtered_by_gene',
                     'Interaction_pathway_gene': 'n_interaction_filtered_pathway_neighbors_by_gene',
                     'Interaction_pathway_disease': 'n_interaction_filtered_pathway_neighbors_by_disease',
                     'Ortholog_zebrafish': 'n_ortholog_by_gene_zebrafish_only_hp',
                     'Between_species_paralog_zebrafish': 'n_betw_paralog_by_gene_zebrafish_only_hp',
                     'Between species paralog': 'n_betw_paralog_by_gene_only_hp',
                     'num_cand': 'n_interaction_filtered_neighbor_num_cand',
                     'num_cand_2': 'n_interaction_filtered_2_neighbor_num_cand',
                     'Pathway': 'n_pathway_by_gene'
                    }

        gene_ref = pickle.load(open(os.path.join(__settings__.APP_HOME, "data", "monarch_data", "gene_ref.pkl"), 'rb'))
        pheno_ref = pickle.load(open(os.path.join(__settings__.APP_HOME, "data", "monarch_data", "pheno_ref.pkl"), 'rb'))
        phenos = set([pheno_ref[x] for x in open(os.path.join(outdir, "%s.trace.txt" %(p_name))).readlines()[0].split(';')[2].split('=')[-1].replace(':','_').split(',') if x in pheno_ref ])
        sim = {}
        net = pickle.load(open(os.path.join(outdir,"%s.pkl" %(p_name)), 'rb'))
        if gene not in net:
            return sim
        for i in net[gene]:
            if type(net[gene][i]) != list:
                continue
            if not (i in n_feature_mappings and n_feature_mappings[i] in feature_names):
                continue 
            neighbor = "%s*%s" %(net[gene][i][1], net[gene][i][2])
            if neighbor in gene_ref and gene_ref[neighbor][0] in phrank._disease_pheno_map:
                val =  phrank.compute_phenotype_match(phrank._disease_pheno_map[gene_ref[neighbor][0]], phenos)
                if "_n" in i:
                    i = i[:-2]
                sim[val] = [net[gene][i][1], i]
        return sim


    def compute_rank(self, p_name, outdir, phrank, feature_names):

        used_genes = set()
        solve_proba = -1.0
        output = open(os.path.join(outdir, "result/" + p_name + "_inpherNet_result.tsv"), 'w')
        output.write("Candidate Gene\tInpherNet Score\tNeighbors(sorted by relevance, left to right)\n")
        count = 1
        same = set()
        curr = -1
        for i, (proba, entry) in enumerate(self.sorted_entries):
            if curr == -1:
                curr = proba
            if not curr == proba:
                count += len(same)
                same = set()
                curr = proba
            same.add(entry.gene_name)
            sim = self.compute_similarity_score( p_name, outdir, phrank, entry.gene_name, feature_names) 
            if not sim:
                output.write(str(count) + "\t" + entry.gene_name + "\t" + str(proba) + "\n")
            else:
                val = ""
                for j in sorted(sim.keys(), reverse=True):
                    val += "%s(%s), " %(sim[j][0], sim[j][1])
                output.write(str(count) + "\t" + entry.gene_name + "\t" + str(proba) + "\t" + val[:-2] + "\n")
        output.close()
        return

    def num_genes_to_look_at(self):
        used_genes = set()
        for proba, entry in self.sorted_entries:
            if proba < 0.95:
                break
            used_genes.add(entry.gene_name)
        return len(used_genes)


def collect_performance(patients):
    ranks = []
    # logger.info("Trying cutoff %f" % cutoff)
    for patient in patients:
        rank = patient.compute_rank()
        ranks.append(rank)
    return ranks


def collect_training_features_labels(patients, subsample_negatives=1):
    labels = [x for p in patients for x in p.train_labels]
    features = [x for p in patients for x in p.train_features]
    index = list(range(len(labels)))
#    shuffle(index)
    rv_labels = []
    rv_features = []
    count_negatives = 0
    pos = 0
    neg = 0
    for i in index:
       if labels[i] == 0:
            if i % subsample_negatives == 0:
                rv_labels.append(labels[i])
                rv_features.append(features[i])
                neg += 1
            count_negatives += 1
       else:
            rv_labels.append(labels[i])
            rv_features.append(features[i])
            pos += 1
    print ("positive cases: %d, negative cases: %d" %(pos, neg))
    return rv_features, rv_labels


def train_and_evaluate():
    fake_solutions_filename = sys.argv[1]
    fake_train_features_path = sys.argv[2]
    real_test_features_path = sys.argv[3] 
    name = sys.argv[4]
    outdir = sys.argv[5]

    solution = parse_solution("%s\tN/A" %(name))
    real = Patient(solution=solution)
    logger.info("loading features")

    feature_names = [
                     "mcap_100bp",
                     "gene_mcap",
                     "gene_rvis",
                     "gene_pli",
                     "average_allele_count",
                     'n_paralog_by_disease',
                     'n_ortholog_by_gene_only_hp',
                     'n_interaction_filtered_by_disease',
                     'num_variants_in_gene',
                     'n_ortholog_by_gene_zebrafish_only_hp',
                     'n_betw_paralog_by_gene_zebrafish_only_hp',
                     'n_betw_paralog_by_gene_only_hp', 
                     'n_interaction_filtered_neighbor_num_cand',
                     'n_interaction_filtered_2_neighbor_num_cand',
                     'n_pathway_by_gene'
                        ]

    try:
        real.load_test_features(feature_names=feature_names, test_features_path=real_test_features_path)
        real.load_test_index_to_entry(test_features_path=real_test_features_path, features=feature_names)
    except OSError as e:
        if e.errno == errno.ENOENT:
            logger.info("Omitting patient %s (%s)" % (p.name, str(e)))
        else:
            raise

    if( len(sys.argv) == 7 ):
        print("Found pretrained file, loading")
        clf = pickle.load(open(sys.argv[6], 'rb'))
    else: 
        fake_solutions = [parse_solution(x) for x in tail(fake_solutions_filename, skip_if_startswith='#')]
        fake_patients = []
        for solution in fake_solutions:
            p = Patient(solution=solution)
            fake_patients.append(p)

        loaded_fake_patients = []
        for p in fake_patients:
            try:
                p.load_train_features(feature_names=feature_names, train_features_path=fake_train_features_path)
                p.load_train_labels(train_features_path=fake_train_features_path)
                loaded_fake_patients.append(p)
            except OSError as e:
                if e.errno == errno.ENOENT:
                    logger.info("Omitting patient %s; causal gene not in the candidate" % p.name)
                else:
                    raise
        fake_patients = loaded_fake_patients
        print( "Initial total patients: %d" %(len(fake_patients)))
        logger.info("features loaded")
        logger.info("Running on %d patients with positives" % len(fake_patients))
        train_features, train_labels = collect_training_features_labels(fake_patients, subsample_negatives=1)
        logger.info("Have %d positives, %d negatives" % (train_labels.count(1), train_labels.count(0)))
        clf = GradientBoostingClassifier(n_estimators=400, learning_rate=0.01, loss='deviance',  max_depth=6, random_state=0)
        clf.fit(X=train_features, y=train_labels)
        if len(sys.argv) > 6:
            pickle.dump(clf, open(os.path.join(__settings__.DATA_DIR, sys.argv[6]), 'wb'))
        else:
            pickle.dump(clf, open(os.path.join(__settings__.DATA_DIR, "saved_classifier.pkl"),'wb'))
    real.run(clf=clf)
    p = pickle.load( open(os.path.join(__settings__.DATA_DIR, "monarch_data", "upheno_phrank.pkl"), "rb"))
    real.compute_rank(real.name, outdir,  p, feature_names)

def main():
    train_and_evaluate()


if __name__ == "__main__":
    logger.info("Starting")
    main()


