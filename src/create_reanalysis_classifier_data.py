#! /usr/bin/env python3

import sys
import os
from collections import namedtuple
from sklearn.linear_model import LogisticRegression
# from sklearn.metrics import classification_report
# from sklearn.ensemble import GradientBoostingClassifier
# from sklearn.neural_network import MLPClassifier
# from sklearn.linear_model import SGDClassifier
import numpy as np
from collections import defaultdict
import pickle
import datetime
import logging

from util import tail, read_tsv
from patients.phenotyper import get_phenotyper
import __settings__

handler = logging.StreamHandler(sys.stderr)
logger = logging.getLogger(__file__)
logger.addHandler(handler)
level = logging.getLevelName('DEBUG')
logger.setLevel(level)


TraceEntry = namedtuple("TraceEntry", ["num_affected",
			"ensembl_id",
			"gene_name",
			"average_mcap",
			"mcap_100bp",
                        "gene_mcap",
                        "gene_rvis_score",
                        "gene_pli_score",
                        "average_allele_count",
			"n_paralog_by_disease",
                        "n_ortholog_by_gene_only_hp",
                        "n_betw_paralog_by_gene_only_hp", 
                        "n_interaction_filtered_by_disease", 
                        "n_interaction_filtered_by_gene",
			"num_variants_in_gene", 
                        "n_ortholog_by_gene_zebrafish_only_hp",
                        "n_betw_paralog_by_gene_zebrafish_only_hp",
                        "n_interaction_filtered_neighbor_num_cand",
                        "n_interaction_filtered_2_neighbor_num_cand",
                        "n_pathway_by_gene"
                         ])


def make_entry(line):
    # logger.info('\n'.join(tup))
    # logger.info(len(tup))
    entry_map = {}
    components = line.split(";")
    for component in components:
        component = component.split('=')
        name = component[0]
        value = component[1]
        entry_map[name] = value
    trace_entry = TraceEntry(num_affected=1,
                             gene_name=entry_map['gene_name'],
                             ensembl_id=entry_map['eid'],
                             gene_rvis_score=float(entry_map['gene_rvis_score']),
                             average_mcap=float(entry_map['average_mcap']) if entry_map['average_mcap'] != "None" else None,
                             mcap_100bp=float(entry_map['mcap_100bp']) if entry_map['mcap_100bp'] != "None" else None,
                             gene_mcap=float(entry_map['gene_mcap']) if entry_map['gene_mcap'] != "None" else None,
                             gene_pli_score=float(entry_map['gene_pli_score']),
                             average_allele_count=float(entry_map['average_allele_count']),
		     	     n_paralog_by_disease=float(entry_map['n_paralog_by_disease']),
                	     n_ortholog_by_gene_only_hp=float(entry_map['n_ortholog_by_gene_only_hp']),
                	     n_betw_paralog_by_gene_only_hp=float(entry_map['n_betw_paralog_by_gene_only_hp']),
                	     n_interaction_filtered_by_disease=float(entry_map['n_interaction_filtered_by_disease']),
               		     n_interaction_filtered_by_gene=float(entry_map['n_interaction_filtered_by_gene']),
			     num_variants_in_gene=int(entry_map['num_variants_in_gene']),
               		     n_ortholog_by_gene_zebrafish_only_hp=float(entry_map['n_ortholog_by_gene_zebrafish_only_hp']),
               		     n_betw_paralog_by_gene_zebrafish_only_hp=float(entry_map['n_betw_paralog_by_gene_zebrafish_only_hp']),
                	     n_interaction_filtered_neighbor_num_cand = int(float(entry_map['n_interaction_filtered_neighbor_num_cand'])),
                	     n_interaction_filtered_2_neighbor_num_cand = int(float(entry_map['n_interaction_filtered_2_neighbor_num_cand'])),
                             n_pathway_by_gene = float(entry_map['n_pathway_by_gene'])
                   )
    return trace_entry


class EntryList:

    _entries = []

    def __init__(self, entries):
        self._entries = entries

    def up_to_date(self, max_year, max_month, throw_unknown_dates=True):
        rv_entries = []
        for entry in self._entries:
            if throw_unknown_dates and (entry.year == -1 or entry.month == -1):
                continue
            if (entry.year, entry.month) <= (max_year, max_month):
                rv_entries.append(entry)
        return EntryList(rv_entries)

    def from_date(self, min_year, min_month, throw_unknown_dates=True):
        rv_entries = []
        for entry in self._entries:
            if throw_unknown_dates and (entry.year == -1 or entry.month == -1):
                continue
            if (entry.year, entry.month) >= (min_year, min_month):
                rv_entries.append(entry)
        return EntryList(rv_entries)

    def get_gene_rank(self, entry):
        self._entries = sorted(self._entries, key=lambda x: x.match_score)
        entry_index = self._entries.index(entry)
        entries_ahead = self._entries[entry_index:]
        gene_set = set(a.ensembl_id for a in entries_ahead)
        return len(gene_set)

    def with_entry(self, entry):
        return EntryList(self._entries + [entry])

    def __iter__(self):
        return iter(self._entries)

    def __len__(self):
        return len(self._entries)


def square_kernel(tup):
    rv = []
    for x in tup:
        rv.append(x)
    for x in tup:
        for y in tup:
            rv.append(x * y)
    return tuple(rv)


def id_kernel(tup):
    return tup


def get_max_historical_match_percent(entry_phenos, previous_phenos,
                                     phenomatcher_date_x, phenomatcher_date_y):
    max_match_score = -1
    for previous in previous_phenos:
        score = get_phenotyper(phenomatcher_date_x,
                               phenomatcher_date_y).phenotype_score(previous,
                                                                    entry_phenos)
        max_match_score = max(max_match_score, score)
    max_current_score = get_phenotyper(phenomatcher_date_x,
                                       phenomatcher_date_y).phenotype_score(entry_phenos,
                                                                            entry_phenos)
    if max_current_score > 0.0:
        return max_match_score / max_current_score
    return 0.0


def is_variant_destructive(entry):
    numbers = [1.0 if (t == "frameshift" or t == "splicing" or t == "stopgain" or t == "stoploss") else 0.0
               for t in entry.variant_types]
    return sum(numbers) / len(numbers)


def get_gene_mcap(entry):
    if entry.gene_mcap is not None:
        return entry.gene_mcap
    return 0.1


def get_mcap_100bp(entry):
    if entry.mcap_100bp is not None:
        return entry.mcap_100bp
    return get_gene_mcap(entry)


def get_average_mcap(entry):
    if entry.average_mcap is not None:
        return entry.average_mcap
    return get_mcap_100bp(entry)


def normalize_and_truncate(number, mean, sd, lower_bound=-1.0, upper_bound=1.0):
    assert lower_bound < upper_bound, (lower_bound, upper_bound)
    number = float(number - mean) / sd
    if number < lower_bound:
        number = lower_bound
    if number > upper_bound:
        number = upper_bound
    return number


feature_map = defaultdict(lambda: [])


def write_feature(feature_name, value):
    feature_map[feature_name].append(value)


def flush_features(patient_id, features_path):
    logger.info( "HERE" )
    for feature_name in feature_map:
        out_filename = os.path.join(features_path, patient_id + '.' + feature_name + '.pkl')
        logger.info (feature_map[feature_name])
        pickle.dump(feature_map[feature_name], open(out_filename, "wb"))


def write_features(entry, gene_to_max_score_over_paper_max,
                   gene_to_max_score_over_patient_max,
                   gene_to_paper_phenos, patient_num_genes,
                   phenomatcher_date_x, phenomatcher_date_y, sub_entries):
    gene_rank = sub_entries.get_gene_rank(entry)
    write_feature('relative_gene_rank', float(gene_rank) / patient_num_genes)

    match_score_over_patient_max = entry.match_score_over_patient_max
    write_feature('match_score_over_patient_max', match_score_over_patient_max)

    match_score_over_paper_max = entry.match_score_over_paper_max
    write_feature('match_score_over_paper_max', match_score_over_paper_max)

    strict_classifier_score = entry.strict_classifier_score
    write_feature('strict_document_relevance', strict_classifier_score)

    max_historical_match = get_max_historical_match_percent(entry_phenos=entry.paper_phenos,
                                                            previous_phenos=gene_to_paper_phenos[entry.ensembl_id],
                                                            phenomatcher_date_x=phenomatcher_date_x,
                                                            phenomatcher_date_y=phenomatcher_date_y)
    write_feature('max_historical_match', max_historical_match)

    max_previous_score_over_patient_max = gene_to_max_score_over_patient_max[entry.ensembl_id]
    write_feature('max_previous_score_over_patient_max', max_previous_score_over_patient_max)
    gene_to_max_score_over_patient_max[entry.ensembl_id] = max(gene_to_max_score_over_patient_max[entry.ensembl_id],
                                                               match_score_over_patient_max)

    max_previous_score_over_paper_max = gene_to_max_score_over_paper_max[entry.ensembl_id]
    write_feature('max_previous_score_over_paper_max', max_previous_score_over_paper_max)
    gene_to_max_score_over_paper_max[entry.ensembl_id] = max(gene_to_max_score_over_paper_max[entry.ensembl_id],
                                                             match_score_over_paper_max)

    average_mcap = get_average_mcap(entry)
    write_feature('average_mcap', average_mcap)

    mcap_100bp = get_mcap_100bp(entry)
    write_feature('mcap_100bp', mcap_100bp)

    gene_mcap = get_gene_mcap(entry)
    write_feature('gene_mcap', gene_mcap)

    pli = entry.gene_pli_score
    write_feature('gene_pli', pli)

    variant_destructive = is_variant_destructive(entry)
    write_feature('variants_destructive', variant_destructive)

    variant_not_destructive = 1 - is_variant_destructive(entry)
    write_feature('variants_not_destructive', variant_not_destructive)

    assert entry.inheritance_vtype == "het" or entry.inheritance_vtype == "OTHER", entry.inheritance_vtype
    variants_het = 1.0 if entry.inheritance_vtype == "het" else 0.0
    write_feature('variants_het', variants_het)

    variants_other = 1 - variants_het
    write_feature('variants_other', variants_other)

    paper_dominant_score = entry.paper_dominant_score
    write_feature('paper_dominant', paper_dominant_score)

    paper_recessive_score = entry.paper_recessive_score
    write_feature('paper_recessive', paper_recessive_score)

    paper_destructive_score = entry.paper_destructive_score
    write_feature('paper_destructive', paper_destructive_score)

    paper_not_destructive_score = entry.paper_not_destructive_score
    write_feature('paper_not_destructive', paper_not_destructive_score)

    overlaps_reported_variant_of_any_type = entry.v_overlaps_reported_variant_of_any_type
    write_feature('overlaps_reported_variant_of_any_type', overlaps_reported_variant_of_any_type)

    overlaps_reported_variant_of_same_type = entry.v_overlaps_reported_variant_of_same_type
    write_feature('overlaps_reported_variant_of_same_type', overlaps_reported_variant_of_same_type)

    v_between_evs = entry.v_between_evs
    write_feature('v_between_evs', v_between_evs)

    # num destructive, not destructive variant features made things worse in the past --- normalize all features (?)

    inv_dist_to_closest_ev = (1.0 / entry.v_dist_to_closest_ev) if (entry.v_dist_to_closest_ev != 0) else 2.0
    write_feature('inv_dist_to_closest_ev', inv_dist_to_closest_ev) # makes things worse

    maxed_dist_to_closest_ev = min(entry.v_dist_to_closest_ev, 20)  # makes things worse
    write_feature('maxed_dist_to_closest_ev', maxed_dist_to_closest_ev)

    v_in_same_exon = entry.v_in_same_exon  # makes things worse
    write_feature('v_in_same_exon', v_in_same_exon)

    gene_rvis_score = entry.gene_rvis_score
    write_feature('gene_rvis', gene_rvis_score)

    right_gene_score = entry.right_gene_score
    write_feature('right_gene', right_gene_score)

    highest_right_gene_score_in_paper = entry.highest_right_gene_score_in_paper
    write_feature('highest_right_gene_score_in_paper', highest_right_gene_score_in_paper)

    diff_highest_right_gene_score_in_paper = highest_right_gene_score_in_paper - right_gene_score
    write_feature('diff_highest_right_gene_score_in_paper', diff_highest_right_gene_score_in_paper)

    num_variants_in_gene = entry.num_variants_in_gene
    write_feature('num_variants_in_gene', normalize_and_truncate(num_variants_in_gene, mean=1.66, sd=3.07))

    num_variants_to_all_genes = entry.num_variants_to_all_genes
    write_feature('num_variants_to_all_genes', normalize_and_truncate(num_variants_to_all_genes, mean=9.52, sd=16.09))

    num_genes_with_variants = entry.num_genes_with_variants
    write_feature('num_genes_with_variants', normalize_and_truncate(num_genes_with_variants, mean=3.96, sd=6.14))

    write_feature("paper_destructive@variants_destructive",
                  paper_destructive_score * variant_destructive)
    write_feature("paper_not_destructive@variants_not_destructive",
                  paper_not_destructive_score * variant_not_destructive)
    write_feature("paper_dominant@variants_het", paper_dominant_score * variants_het)
    write_feature("paper_recessive@variants_other", paper_recessive_score * variants_other)


def featurize_label(patient_id, entries, solution, min_year, min_month,
                    phenomatcher_date_x, phenomatcher_date_y, patient_num_genes,
                    features_path):
    max_year = min_year
    max_month = min_month
    found_first_pmid = False
    labels = []
    index_to_entry = []

    gene_to_paper_phenos = defaultdict(list)
    gene_to_max_score_over_patient_max = defaultdict(lambda: 0.0)
    gene_to_max_score_over_paper_max = defaultdict(lambda: 0.0)
    solving_paper_match_score = -1
    while max_year <= 2020:
        while max_month <= 12:
            logger.info("Doing month %s-%s" % (max_year, max_month))
            if (max_year, max_month) > (solution.first_pmid_year, solution.first_pmid_month):
                logger.info("Breaking search for first PMID after %s-%s" % (solution.first_pmid_year,
                                                                            solution.first_pmid_month))
                break
            sub_entries = entries.up_to_date(max_year, max_month)
            current_entries = sub_entries.from_date(max_year, max_month)
            for entry in current_entries:
                write_features(entry, gene_to_max_score_over_paper_max,
                               gene_to_max_score_over_patient_max,
                               gene_to_paper_phenos, patient_num_genes,
                               phenomatcher_date_x, phenomatcher_date_y,
                               sub_entries)
                index_to_entry.append(entry)
                label = 1 if entry.pmid in solution.first_pmids else 0
                solving_paper_match_score = entry.match_score
                labels.append(label)
                if label == 1:
                    found_first_pmid = True
                    assert max_year == solution.first_pmid_year, (max_year, solution.first_pmid_year)
                    assert max_month == solution.first_pmid_month, (max_month, solution.first_pmid_month)
            max_month += 1
            if found_first_pmid:
                break
            for entry in current_entries:  # this loop has to be after the break for positives augmentation
                gene_to_paper_phenos[entry.ensembl_id].append(entry.paper_phenos)
        if found_first_pmid:
            break
        max_month = 1
        max_year += 1
    if not found_first_pmid:
        logger.info("Warning: never found any first PMID in '%s' for patient '%s'" %
                    (','.join(solution.first_pmids), solution.patient))
    else:
        post_year = solution.first_pmid_year
        post_month = solution.first_pmid_month + 1
        if post_month >= 13:
            post_month = 1
            post_year += 1
        post_solving_entries = entries.from_date(post_year, post_month)
        sub_entries = entries.up_to_date(solution.first_pmid_year,
                                         solution.first_pmid_month)
        augmented_positives = [x for x in post_solving_entries
                               if x.gene_name == solution.solution
                               and x.match_score >= solving_paper_match_score]
        for entry in augmented_positives:
            logger.info("Found augmented positive %s" % entry.pmid)
            sub_entries_with_entry = sub_entries.with_entry(entry)
            write_features(entry, gene_to_max_score_over_paper_max,
                           gene_to_max_score_over_patient_max,
                           gene_to_paper_phenos, patient_num_genes,
                           phenomatcher_date_x, phenomatcher_date_y,
                           sub_entries_with_entry)
            labels.append(1.0)
            index_to_entry.append(entry)
    flush_features(patient_id=patient_id, features_path=features_path)
    labels_indices = [labels, index_to_entry]
    patient_labels_output = os.path.join(features_path, patient_id + '.labels.pkl')
    pickle.dump(labels_indices, open(patient_labels_output, "wb"))


def split_cv(iterable, num):
    lst = [x for x in iterable]  # [:100]
    splits = [lst[i::num] for i in range(num)]
    for i in range(len(splits)):
        yield [x for y in splits[0:i] + splits[i + 1:] for x in y], splits[i]


def calculate_months_to_solve(min_year_month, min_solve_year_month):
    date1 = min_year_month[0] * 12 + min_year_month[1]
    date2 = min_solve_year_month[0] * 12 + min_solve_year_month[1]
    return date2 - date1 + 1  # let's do min 1 month ... otherwise division 0 errors ...


def get_patient_num_genes(filename):
    logger.info("Getting number of patient genes from %s" % filename)
    candidate_genes = set()
    for row in read_tsv(filename):
        assert row[7].startswith('ENSG'), row[7]
        candidate_genes.add(row[7])
    return len(candidate_genes)


def main():
    solutions_filename = sys.argv[1]
    patient_id = sys.argv[2]
    min_year = int(sys.argv[3])
    min_month = int(sys.argv[4])
    phenomatcher_date_x = sys.argv[5]
    phenomatcher_date_y = sys.argv[6]
    features_path = sys.argv[7]
    patient_input = os.path.join(__settings__.APP_HOME, "fake_phenos_real_gts", "traces",
                                 patient_id + ".X2011_Yall.trace.txt")
    logger.info("Taking input from %s" % patient_input)
    patient_num_genes = get_patient_num_genes(os.path.join(__settings__.APP_HOME, "fake_phenos_real_gts",
                                                           patient_id + ".alleles.txt"))

    solutions = [parse_solution(x) for x in tail(solutions_filename, skip_if_startswith='#')]

    patient_solutions = [s for s in solutions if s.patient == patient_id]
    assert len(patient_solutions) == 1, patient_solutions
    solution = patient_solutions[0]

    if solution.comment.startswith("?"):
        logger.info("Omitting patient %s (%s); comment: %s" % (solution.patient,
                                                               solution.solution,
                                                               solution.comment))
        return
    logger.info("Featurizing patient %s" % solution.patient)
    entries = EntryList([make_entry(line) for line in tail(patient_input)])
    featurize_label(patient_id=patient_id,
                    entries=entries, solution=solution,
                    min_year=min_year, min_month=min_month,
                    phenomatcher_date_x=phenomatcher_date_x,
                    phenomatcher_date_y=phenomatcher_date_y,
                    patient_num_genes=patient_num_genes,
                    features_path=features_path)


if __name__ == "__main__":
    logger.info("Starting")
    main()
