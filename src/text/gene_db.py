#! /usr/bin/env python3

from collections import defaultdict
import re
import os
import sqlite3 as lite

import __settings__
from text import util
from text import tuples
from text import extractor_util

def recreate():
    filename = os.path.join(__settings__.APP_HOME, 'onto/genes/genes.sqlite3')
    print('genes_db filename: %s' % filename)
    open(filename, 'w').close()
    conn = lite.connect(filename)
    cur = conn.cursor()
    cur.execute("CREATE TABLE eid_map (name text primary key, data text)")
    cur.execute("CREATE TABLE eid_map_permuted (name text primary key, data text)")
    bad_genes = util.read_bad_genes()
    bad_proteins = util.read_bad_proteins()
    eid_map, eid_map_permuted = gene_symbol_to_ensembl_id_map(bad_genes, bad_proteins)
    for gene_name in eid_map:
        values = eid_map[gene_name]
        text_value = '\n'.join(extractor_util.get_tsv_output(x) for x in values)
        cur.execute("INSERT INTO eid_map (name, data) VALUES (?, ?)", (gene_name, text_value))
    for gene_name_set in eid_map_permuted:
        values = eid_map_permuted[gene_name_set]
        key = ' '.join(sorted(gene_name_set))
        text_value = '\n'.join(extractor_util.get_tsv_output(x) for x in values)
        cur.execute("INSERT INTO eid_map_permuted (name, data) VALUES (?, ?)", (key, text_value))
    conn.commit()
    conn.close()

def recreate_eid_to_name():
    filename = os.path.join(__settings__.APP_HOME, 'onto/genes/eid_to_name.sqlite3')
    open(filename, 'w').close()
    conn = lite.connect(filename)
    cur = conn.cursor()
    cur.execute("CREATE TABLE eid_to_name (eid text primary key, canonical_name text)")
    with open(os.path.join(__settings__.APP_HOME, 'onto/genes/ensembl_genes_with_names.tsv')) as f:
        for line in f:
            eid, canonical_name, orig_name, mapping_type = line.rstrip('\n').split('\t')
            if mapping_type != 'HGNC_SYMBOL':
                continue
            cur.execute("INSERT OR REPLACE INTO eid_to_name (eid, canonical_name) VALUES (?, ?)", (eid, canonical_name))
    conn.commit()
    conn.close()

def gene_symbol_to_ensembl_id_map(bad_genes, bad_proteins):
    with open(os.path.join(__settings__.APP_HOME, 'onto/genes/ensembl_genes_with_names.tsv')) as f:
        eid_map = defaultdict(set)
        eid_map_permuted = defaultdict(set)
        for line in f:
            eid, canonical_name, orig_name, mapping_type = line.rstrip('\n').split('\t')
            orig_name = orig_name.strip()
            gene_name = orig_name.split()
            gene_name = util.normalize_gene_name(gene_name, mapping_type)
            gene_name = util.replace_some_greek_letters(gene_name)
            add = True
            # print(mapping_type, file=sys.stderr)
            if mapping_type.startswith('PROTEIN'):
                if gene_name in bad_proteins or orig_name in bad_proteins:
                    add = False
                for bad_gene in bad_genes:
                    if re.search(bad_gene, gene_name, flags=re.IGNORECASE) or re.search(bad_gene, orig_name, flags=re.IGNORECASE):
                        add = False
                        # print("Omitting 2 %s" % orig_name, file=sys.stderr)
                        break
            else:
                for bad_gene in bad_genes:
                    if re.search(bad_gene, gene_name, flags=re.IGNORECASE) or re.search(bad_gene, orig_name, flags=re.IGNORECASE):
                        add = False
                        # print("Omitting 3 %s" % orig_name, file=sys.stderr)
                        break
            if add is True:
                eid_map[orig_name].add(tuples.GeneName(eid, canonical_name, mapping_type))
                eid_map[gene_name].add(tuples.GeneName(eid, canonical_name, mapping_type))
                eid_map[gene_name.replace(' ', '')].add(tuples.GeneName(eid, canonical_name, mapping_type))
                if mapping_type.startswith('PROTEIN'):
                    eid_map_permuted[frozenset(gene_name.split())].add(tuples.GeneName(eid, canonical_name, mapping_type))
    return eid_map, eid_map_permuted

class EidToNameDB:
    
    def __init__(self):
        self.filename = os.path.join(__settings__.APP_HOME, 'data/onto/genes/eid_to_name.sqlite3')
        self.conn = lite.connect(self.filename)
        self.cur = self.conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
    
    def close(self):
        self.conn.close()
    
    def query(self, eid):
        results = self.cur.execute("SELECT canonical_name FROM eid_to_name WHERE eid = ?", [eid])
        for r in results:
            return r[0]

class EnsemblGeneDB:
    
    contained = set()
    not_contained = set()
    matched = {}

    def __init__(self):
        self.filename = os.path.join(__settings__.APP_HOME, 'onto/genes/genes.sqlite3')
        self.conn = lite.connect(self.filename)
        self.cur = self.conn.cursor()

    def close(self):
        self.conn.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def contains(self, word):
        if word in self.contained:
            return True
        if word in self.not_contained:
            return False
        results = self.cur.execute("SELECT name FROM eid_map WHERE name = ?", [word])
        for _ in results:
            self.contained.add(word)
            return True

        permuted = word.split()
        if len(permuted) >= 3:
            key = ' '.join(sorted(permuted))
            results = self.cur.execute("SELECT name FROM eid_map_permuted WHERE name = ?", [key])
            for _ in results:
                self.contained.add(word)
                return True
        self.not_contained.add(word)
        return False

    def match(self, word):
        if word in self.matched:
            return self.matched[word]
        results = self.cur.execute("SELECT data FROM eid_map WHERE name = ?", [word])
        rv = []
        for r in results:
            gene_names = tuples.read_text(r[0].split('\n'),
                tuples.geneNameParser,
                tuples.GeneName)
            for gene_name in gene_names:
                rv.append(gene_name)
        if rv:
            self.matched[word] = set(rv)
            return set(rv)

        permuted = word.split()
        if len(permuted) >= 3:
            key = ' '.join(sorted(permuted))
            results = self.cur.execute("SELECT data FROM eid_map_permuted WHERE name = ?", [key])
            for r in results:
                gene_names = tuples.read_text(r[0].split('\n'),
                    tuples.geneNameParser,
                    tuples.GeneName)
                for gene_name in gene_names:
                    rv.append(gene_name)
        self.matched[word] = set(rv)
        return set(rv)
