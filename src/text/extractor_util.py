from __future__ import print_function

import re
import sys

def tsv_string_to_list(s, func=lambda x : x, sep=' '):
    if s.strip() == "":
        return []
    else:
        split = s.split(sep)
    return [func(x) for x in split]

def tsv_string_to_listoflists(s, func=lambda x : x, sep1=' ', sep2='|~|'):
    return tsv_string_to_list(s, func=lambda x : tsv_string_to_list(x, func=func, sep=sep1), sep=sep2)

def bool_parser(b):
    b = b.strip()
    if b == 't':
        return True
    elif b == 'f':
        return False
    elif b == 'NULL' or b == '\\N':
        return None
    else:
        raise Exception("Unrecognized bool type in RowParser:bool_parser: %s" % b)

# NOTE: array_to_string doesn't work well for bools!    Just pass psql array out!
RP_PARSERS = {
    'text' : lambda x : str(x.replace('\n', ' ')),
    'text[]' : lambda x : tsv_string_to_list(x.replace('\n', ' ')),
    'int' : lambda x : int(x.strip()),
    'int[]' : lambda x : tsv_string_to_list(x, func=lambda x: int(x.strip())),
    'int[][]' : lambda x : tsv_string_to_listoflists(x, func=lambda x: int(x.strip())),
    'boolean' : lambda x : bool_parser(x),
    'boolean[]' : lambda x : tsv_string_to_list(x, func=bool_parser)
}

class Row:
    def __init__(self):
        self.names = []
        self.vals = []
    def __str__(self):
        return '<Row(' + ', '.join("%s=%s" % x for x in zip(self.names, self.vals)) + ')>'
    def __repr__(self):
        return str(self)
    def items_list(self):
        return [v for v in self.vals]
    def add_value(self, field_name, val):
        self.names.append(field_name)
        self.vals.append(val)
    
class RowParser:
    def __init__(self, fields):
        self.fields = fields

    def parse_tsv_row(self, line):
        row = Row()
        cols = line.split('\t')
        for i, col in enumerate(cols):
            field_name, field_type = self.fields[i]
            if field_type in RP_PARSERS:
                val = RP_PARSERS[field_type](col)
            else:
                raise Exception("Unsupported type %s for RowParser class- please add.")
            row.add_value(field_name, val)
        return row

def get_tsv_output(out_record, pmid=None):
    values = []
    if pmid is not None:
        values.append(pmid)
    for x in out_record:
        if isinstance(x, list) or isinstance(x, tuple):
            cur_val = ' '.join([str(s) for s in x])
        elif x is None:
            cur_val = '\\N'
        else:
            cur_val = x
        values.append(cur_val)
    return '\t'.join(str(x) for x in values).encode('ascii', 'ignore').decode('ascii')

def print_tsv_output(out_record, f, pmid=None):
    print(get_tsv_output(out_record, pmid), file=f)