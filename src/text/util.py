import re
from collections import defaultdict
import traceback
import os
import unicodedata
import sys
import linecache
import sqlite3 as lite

import __settings__

def rgx_comp(strings=[], rgxs=[]):
    r = r'|'.join(re.escape(w) for w in strings)
    if len(rgxs) > 0:
        if len(strings) > 0:
            r += r'|'
        r += r'(' + r')|('.join(rgxs) + r')'
    return r

def replace_some_greek_letters(s):
    s = s.replace('alpha', 'a')
    s = s.replace('beta', 'b')
    s = s.replace('gamma', 'g')
    s = s.replace('α', 'a')
    s = s.replace('𝛼', 'a')
    s = s.replace('β', 'b')
    s = s.replace('γ', 'g')
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')

no_alnum = re.compile(r'[^a-zA-Z0-9_]+')

def normalize_gene_name(symbol, mapping_type):
    # this costs a TON of time and doesn't actually get as that much closer, I think ...
    # symbol = replace_some_greek_letters_list(symbol)
    if not mapping_type.startswith('PROTEIN'):
        return ' '.join(symbol)
    if len(symbol) == 1 and any(x.isupper() for x in symbol[0][1:]):
        return symbol[0]
    for j in range(len(symbol)):
        symbol[j] = symbol[j].lower()
    symbol = ' '.join(symbol)
    rv = ' '.join(no_alnum.sub(' ', symbol).split())
    return rv

def rgx_mult_search(phrase, strings, rgxs, orig_strings, orig_rgxs, flags=re.I):
    for i, s in enumerate(strings):
        try:
            regex = re.escape(s)
            if re.search(regex, phrase, flags):
                return orig_strings[i]
        except Exception:
            traceback.print_exc()
            print(regex)
    for i, s in enumerate(rgxs):
        try:
            regex = s
            if re.search(regex, phrase, flags):
                return orig_rgxs[i]
        except Exception:
            traceback.print_exc()
            print(regex)
    return None


class Dag:

    buHpoDag = None
    tdHpoDag = None

    """Class representing a directed acyclic graph."""
    def __init__(self, nodes, edges):
        self.nodes = nodes
        self.node_set = set(nodes)
        self.buHpoDag = edges    # edges is dict mapping child to list of parents
        self.tdHpoDag = defaultdict(lambda: set())
        for child, parents in self.buHpoDag.items():
            for parent in parents:
                self.tdHpoDag[parent].add(child)
        self._has_child_memoizer = defaultdict(dict)

    def has_child(self, parent, child):
        """Check if child is a child of parent."""
        if child not in self.node_set:
            raise ValueError('"%s" not in the DAG.' % child)
        if parent not in self.node_set:
            raise ValueError('"%s" not in the DAG.' % parent)
        if child == parent:
            return True
        if child in self._has_child_memoizer[parent]:
            return self._has_child_memoizer[parent][child]
        for node in self.buHpoDag[child]:
            if self.has_child(parent, node):
                self._has_child_memoizer[parent][child] = True
                return True
        self._has_child_memoizer[parent][child] = False
        return False


def read_hpo_dag(dag=os.path.join(__settings__.APP_HOME, 'onto/hpo/hpoDag.txt')):
    with open(dag) as f:
        nodes = []
        edges = {}
        for line in f:
            line = line.strip().split('\t')
            child = line[0]
            parent = line[1]
            if child not in nodes:
                nodes.append(child)
            if parent not in nodes:
                nodes.append(parent)
            if parent not in edges:
                edges[parent] = []
            if child in edges:
                edges[child].append(parent)
            else:
                edges[child] = [parent]
        return Dag(nodes, edges)


def get_hpo_phenos(hpo_dag, parent='HP:0000118', exclude_parents=[]):
    return [hpo_term for hpo_term in hpo_dag.nodes
            if (hpo_dag.has_child(parent, hpo_term)
            and all([not hpo_dag.has_child(p, hpo_term) for p in exclude_parents]))]


def read_text(text):
    rv = []
    for line in text:
        rv.extend(line.split(' '))
    return rv

def read_doc(filename):
    rv = []
    with open(filename, encoding="utf-8") as f:
        for line in f:
            words = line.strip().split(' ')
            rv.extend(words)
    return rv

def read_sentences(filename):
    rv = []
    with open(filename, encoding="utf-8") as f:
        for line in f:
            words = line.strip().split(' ')
            rv.append(words)
    return rv

def build_words_to_sent_map(filename):
    rv = {}
    start_wordidx = 0
    with open(filename, encoding="utf-8") as f:
        for linenum, line in enumerate(f):
            end_wordidx = start_wordidx + len(line.rstrip('\n').split(' ')) - 1
            for wordidx in range(start_wordidx, end_wordidx):
                rv[wordidx] = linenum
            start_wordidx = end_wordidx
    return rv

def build_sent_to_start_word(filename):
    rv = {}
    start_wordidx = 0
    with open(filename, encoding="utf-8") as f:
        for linenum, line in enumerate(f):
            rv[linenum] = start_wordidx
            end_wordidx = start_wordidx + len(line.rstrip('\n').split(' ')) - 1
            start_wordidx = end_wordidx
    return rv

def load_canon_pheno_names():
    canon_names = {}
    rows = [line.split('\t') for line in open(os.path.join(__settings__.APP_HOME, 'onto/hpo/pheno_terms.tsv'))]
    for row in rows:
        hpoid, phrase, entry_type = [x.strip() for x in row]
        if hpoid not in canon_names and entry_type == 'EXACT':
            canon_names[hpoid] = phrase
    return canon_names

def read_bad_genes(filename=os.path.join(__settings__.APP_HOME, 'onto/genes/bad_genes.txt')):
    rv = []
    with open(filename) as f:
        for line in f:
            line = line.strip()
            rv.append(line)
    return rv

def read_bad_proteins(filename=os.path.join(__settings__.APP_HOME, 'onto/genes/bad_proteins.txt')):
    rv = []
    with open(filename) as f:
        for line in f:
            line = line.strip()
            rv.append(line)
            rv.append(line.lower())
    return rv

def read_manual_bad(filename=os.path.join(__settings__.APP_HOME, 'onto/genes/manual_bad_genes.txt')):
    rv = {}
    with open(filename) as f:
        for line in f:
            line = line.strip().split('\t')
            genes = tuple([w.strip() for w in line[0].strip().split(',')])
            cues = [w.strip() for w in line[1].strip().split(',')]
            rv[genes] = cues
    return rv

def get_input(basedir, filename, other_source = None):
    pmid = filename.strip().split('.')[0]
    if other_source is None:
        return os.path.join(basedir, '/'.join(pmid[:4]), filename)
    filename = filename.strip().split('.')
    filename = '.'.join([filename[0], other_source, filename[2]])
    return os.path.join(basedir, '/'.join(pmid[:4]), filename)

made_directories = set()

def make_output(basedir, filename):
    pmid = filename.strip().split('.')[0]
    path_to_make =  os.path.join(basedir, '/'.join(pmid[:4]))
    if path_to_make not in made_directories:
        try:
            os.makedirs(path_to_make, exist_ok=True)
            # exception can still happen when directory access mode is not as expected
            # but is much rarer
        except FileExistsError:
            pass
        made_directories.add(path_to_make)
    return os.path.join(path_to_make, filename)

def get_db(filename):
    conn = lite.connect(filename)
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS data(pmid " + \
                " text, source text, text text, " + \
                "PRIMARY KEY (pmid, source))")
    return conn, cur

def write_db(db, pmid, source, text):
    conn, cur = db
    cur.execute("INSERT OR REPLACE INTO data " +
                "(pmid, source, text) VALUES (?, ?, ?)", 
                [pmid, source, text])

def close_db(db):
    conn, cur = db
    conn.commit()
    conn.close()
    
short_words = { 'the', 'and', 'or', 'at', 'in', 'see', 'as',
                'an', 'data', 'for', 'not', 'our', 'ie', 'to',
                'eg', 'one', 'age', 'on', 'center', 'right', 'left',
                'from', 'based', 'total', 'via', 'but', 'resp', 'no' }


def count_lines(filename):
    with open(filename, encoding="utf-8") as f:
        for i, l in enumerate(f):
            pass
    return i+1

ensembl_gene2pubtator = os.path.join(__settings__.APP_HOME, 'onto/genes/ensembl_gene2pubtator.sqlite3')
# TODO HACK This is SUPER BAD ...
# ensembl_gene2pubtator_wcl = 7995385
# better, but slower:
# ensembl_gene2pubtator_wcl = count_lines(ensembl_gene2pubtator)

gene2pubtator_map = {}
gene2pubtator_conn = None
gene2pubtator_cur = None

def gene2pubtator(pmid):
    global gene2pubtator_conn
    global gene2pubtator_cur
    if pmid in gene2pubtator_map:
        return gene2pubtator_map[pmid]
    if gene2pubtator_conn is None:
        gene2pubtator_conn = lite.connect(ensembl_gene2pubtator)
        gene2pubtator_cur = gene2pubtator_conn.cursor()
    data = gene2pubtator_cur.execute('SELECT data FROM pubtator WHERE pmid = ?', [pmid])

    rv = defaultdict(lambda: set())
    for row in data:
        lines = row[0].split('\n')
        for line in lines:
            eid, canonical_name, gene_name, mapping_type = line.rstrip('\n').split('\t')
            rv[gene_name].add((eid, canonical_name, mapping_type))
            normalized_gene_name = normalize_gene_name(gene_name.strip().split(), 'PROTEIN')
            rv[normalized_gene_name].add((eid, canonical_name, mapping_type))
    gene2pubtator_map[pmid] = rv
    return rv
