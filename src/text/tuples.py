from __future__ import print_function

import collections

from text import extractor_util

PhenoMentionCore = [("wordidxs", "int[]"), 
                    ("words", "text[]"), 
                    ("hpo_id", "text"), 
                    ("canon_name", "text")]
GeneMentionCore = [("gene_name", "text"), 
                   ("wordidxs_start", "int[]"), 
                   ("wordidxs_end", "int[]"), 
                   ("num_mentions", "int"), 
                   ("mapping_type", "text"),
                   ("eids", "text[]"),
                   ("surrounding_words_left", "text[]"),
                   ("surrounding_words_right", "text[]"),
                   ("orig_words", "text")]
NonGeneAcronymCore = [("wordidx", "int"), 
                      ("abbrev", "text"), 
                      ("definition", "text[]")]
PhenoAcronymCore = [("wordidx", "int"), 
                    ("hpo_id", "text"), 
                    ("abbrev", "text"), 
                    ("definition", "text")]
RightGeneCore = [("eid", "text")]
GeneNameCore = [("eid", "text"),
            ("canonical_name", "text"),
            ("mapping_type", "text")]

def createNamedTuple(name, core):
    l = [n[0] for n in core]
    return collections.namedtuple(name, l)

PhenoMention = createNamedTuple("PhenoMention", PhenoMentionCore)
GeneMention = createNamedTuple("GeneMention", GeneMentionCore)
NonGeneAcronym = createNamedTuple("NonGeneAcronym", NonGeneAcronymCore)
PhenoAcronym = createNamedTuple("PhenoAcronym", PhenoAcronymCore)
RightGene = createNamedTuple("RightGene", RightGeneCore)

GeneName = createNamedTuple("GeneNameCore", GeneNameCore)

phenoMentionParser = extractor_util.RowParser(PhenoMentionCore)
geneMentionParser = extractor_util.RowParser(GeneMentionCore)
nonGeneAcronymParser = extractor_util.RowParser(NonGeneAcronymCore)
phenoAcronymParser = extractor_util.RowParser(PhenoAcronymCore)
rightGeneParser = extractor_util.RowParser(RightGeneCore)

geneNameParser = extractor_util.RowParser(GeneNameCore)

def read_file(filename, parser, RvType):
    text = []
    with open(filename) as f:
        for line in f:
            if line.startswith('#') or line.startswith("ShallowFloat"): # ugly fix, it should only be '#' but I forgot it in one of the version strings ...
                continue
            text.append(line.strip())
    return read_text(text)

def read_text(text, parser, RvType, ignoreTypeErrors=False, ignoreEmptyLines=False):
    for line in text:
        if ignoreEmptyLines:
            if not line:
                continue
        try:
            row = parser.parse_tsv_row(line)
            yield RvType._make(row.items_list())
        except TypeError as e:
            if ignoreTypeErrors:
                pass
            else:
                print(row)
                raise e
        except ValueError as e:
            if ignoreTypeErrors:
                pass
            else:
                print(row)
                raise e
