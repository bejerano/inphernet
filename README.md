# InpherNet

**Welcome to InpherNet!** 

*InpherNet* is a network-based machine learning tool that leverages indirect biologically relevant "neighbors" of the candidate causative genes to accelerate identifying novel monogenic disease-causing candidates.
For more details on InpherNet please refer our [manuscript] (https://www.nature.com/articles/s41436-021-01238-2).
Please see step by step instructions on how to run InpherNet

## Data

Data needed to run InpherNet can be found here: https://doi.org/10.6084/m9.figshare.14058113.v1

## Required environment and packages

1. Python3
2. ANNOVAR (Wang et. al.) can be downloaded from their [website](https://www.openbioinformatics.org/annovar/annovar_download_form.php).
3. Numpy
4. Scikit learn 

## Steps

1. Make sure you have the required environment set up and downloaded
2. In **runner**, change the *ANNOVAR* variable to the path where you downloaded ANNOVAR (i.e., path to table\_annovar.pl and convert2annovar.pl)
3. Save and unzip the data in appropriate paths. InpherNet_data.zip should be stored as **data** in the top directory (i.e., where runner script is) and InpherNet_annovar_data.zip should be stored as **hg19** under $Top_InpherNet_directory/src/build_patient_variants/annovar_pipe.
4. Unzip onto.tar.gz and train.tar.gz under the **data** directory.
5. run using:
	`./runner [path to vcf/variants.txt file] [phenotype file] [output directory] [optional:n]` 

## Input details

1. variants.txt file is the output file InpherNet will make taking the result of running ANNOVAR. This will be stored in your output directory. If you want to rerun your case (e.g., after changing the input phenotype list) and want to skip over the annotation step to save time you can use this file.
2. The phenotype file must be named [patient identifier].phenotypes.txt and contain a comma separated HPO terms in the first line (e.g., HP:0000752,HP:0001249) 
3. If the optional **n** is set, then the classifier is trained again, unless you changed the training data we recommend that you do not set this flag

## Understanding the output

InpherNet results will be stored under [output directory]/result/[patient identifier]\_inpherNet\_result.tsv <br>
This file has four columns:	
	1. Candidate gene rank
	2. Candidate gene symbol
	3. InpherNet score
	4. List of relevant candidate gene neighbors (sorted left to right in importance)

## Example

1. Take steps 1-2 in the "Steps" section
2. In the InpherNet top directory run `./runner example/615120_AGRN.vcf example/615120_AGRN.phenotypes.txt 615120_AGRN`
3. Once it finishes running the result will be stored in 615120\_AGRN/result/615120\_AGRN\_inpherNet\_result.tsv
4. This is a toy example with AGRN as the causative gene. The list of phenotypes in HPO terms can be found in example/615120\_AGRN.phenotypes.txt


